(ns newvoyage.handler
  (:use compojure.core)

  (:require [clojure.java.io :as io]
            [ring.middleware.reload :refer (wrap-reload)] ;; reload temlates
            [ring.middleware.json :as json]
            [ring.middleware.session :as session]
            [ring.middleware.multipart-params :as multipart]

            [compojure.handler :as handler]
            [compojure.route :as route]
            [ixinfestor.core-web :as cw]
            [ixinfestor.core-web-bootstrap :as cwb]

            [ixinfestor.core :as ix]
            [ixinfestor.core-handler :as ixch]

            [newvoyage.web :as w]
            [newvoyage.core :as tc]

            [cemerick.friend :as friend]
            (cemerick.friend [workflows :as workflows]
                             [credentials :as creds])

            [clojure.tools.nrepl.server :as nrepl-server]
            [cider.nrepl :refer (cider-nrepl-handler)]
            )
  )

;;**************************************************************************************************
;;* BEGIN Init web aplication
;;* tag: <init web application>
;;*
;;* description: Инициализация вебприложения
;;*
;;**************************************************************************************************

(defn init []
  (println "INIT SYSTEM")

  (when ix/development?
    (do
      (print "STARTING NREPL -> ")
      (nrepl-server/start-server :port 7888 :handler cider-nrepl-handler)
      (print "[ok]\n")))

  ;;(print "STARTING SCHEDULLER -> ")
  ;; Some code.....
  ;;(print "[ok]")

  )

;; END Init web aplication
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN URL ROUTES
;;* tag: <url route system>
;;*
;;* description: Управление вызовами
;;*
;;**************************************************************************************************

(defroutes app-routes


  ;; ----------------------------------------------------------------------------------------------
  (ANY "/" request
       (-> request
           w/page-main
           ring.util.response/response
           (ring.util.response/header "Content-Type" "text/html; charset=utf-8")))


  (ANY "/for-price" request (w/page-for-price request))

  (ANY "/doc/:id*" request (w/page-doc request))

  (ANY "/taginfo/:id*" request (w/page-tag request))

  (ANY "/articles" request (w/page-articles request))

  (ANY "/search" request (w/page-search request))

  (ANY "/tour-selection" request (w/page-tour-selection request))


  ;; старые ссылки !!!
  (GET "/vta*" request  (ring.util.response/redirect "/" ))
  (GET "/faces*" request  (ring.util.response/redirect "/" ))
  (GET "/vta/faces*" request  (ring.util.response/redirect "/" ))


  ;; Реиндексация
  (GET "/recalc-all" _
       (ring.util.response/response
        (str (count (map ix/webdoc-save (-> ix/webdoc-select* (korma.core/fields :id :keyname) ix/com-exec ))))))



  ;; ----------------------------------------------------------------------------------------------

  (ixch/routes-ix* #{(:keyname tc/webrole-user)}
                   {:page-ixcms-main-params
                    {:title "CMS New Voyage"}})
  (ixch/routes-file* #{:user} {})
  (ixch/routes-stext* #{:user})
  (ixch/routes-webusers* #{:admin})
  (ixch/routes-tag* #{:user})

  (ixch/routes-webdocs*
   #{:user}
   {:webdoc-entity tc/webdoc
    :webdoc-select* tc/webdoc-select*
    :webdoc-save-fn tc/webdoc-save
    :covertors-fn (fn [webdoc-row]
                    (-> webdoc-row
                                             ;;; SPEC HANDLER
                        (ixch/update-in-if-not-nil? [:price] bigdec)
                        (ixch/update-in-if-not-nil? [:orderindex] ixch/parseLong)
                        (ixch/update-in-if-not-nil? [:showbdate] #(new java.util.Date %))
                        (ixch/update-in-if-not-nil? [:showedate] #(new java.util.Date %))))
    })



  ;; ----------------------------------------------------------------------------------------------

  ;; anonymous
  (GET "/login" [] (hiccup.page/html5 cwb/page-sign-in))
  (GET "/logout" req (friend/logout* (ring.util.response/redirect (str (:context req) "/login"))))


  (route/resources "/")
  (route/not-found "Not Found"))


;; END URL ROUTES
;;..................................................................................................

;;**************************************************************************************************
;;* BEGIN Users and roles autentification
;;* tag: <webusers roles auth>
;;*
;;* description: Аутентификация пользователей
;;*
;;**************************************************************************************************

(defn get-user [username]
  (let [webuser-row (ix/webuser-find-by-username username)]
    (->> webuser-row
         ix/webuserwebrole-own-get-rels-set
         ;;(map keyword)
         ;;set
         (assoc webuser-row :roles))))

;; END Users and roles autentification
;;..................................................................................................
;;**************************************************************************************************
;;* BEGIN Ring middleware
;;* tag: <ring web midleware>
;;*
;;* description: Веб функционал RING
;;*
;;**************************************************************************************************

(def site
  (-> app-routes
      (friend/authenticate {:allow-anon? true
                            :login-uri "/login"
                            :default-landing-uri "/"
                            :unauthorized-handler #(-> (hiccup.page/html5
                                                        [:h2 "You do not have sufficient privileges to access " (:uri %)])
                                                       ring.util.response/response
                                                       (ring.util.response/status 401))
                            :credential-fn (partial creds/bcrypt-credential-fn get-user)
                            :workflows [(workflows/interactive-form)]})
      (session/wrap-session {:cookie-attrs {:max-age (* 3600 24 4)}})
      ring.middleware.keyword-params/wrap-keyword-params
      (json/wrap-json-params   {:bigdecimals? true})
      (json/wrap-json-response {:keywords? true :bigdecimals? true})
      ))

(def app (handler/site site))

;; END Ring middleware
;;..................................................................................................
