(ns newvoyage.web

  (:use hiccup.core)
  (:use hiccup.page)
  (:use hiccup.form)
  (:use hiccup.element)
  (:use hiccup.util)

  (:require [korma.core :as korc]
            [newvoyage.core :as c]

            ;;[postal.core :as postal]
            [ixinfestor.core :as ix]
            [ixinfestor.core-web :as cw]
            [ixinfestor.core-web-bootstrap :as cwb]

            [clj-time.format :as timef]
            [clj-time.coerce :as timec]
            [clj-time.local :as timel]

            [postal.core :as postal]
            )
  )

(def news-date-format (java.text.SimpleDateFormat. "dd.MM.yyyy HH:mm"))


(defn date-day [date]
  (new java.util.Date (- (.getTime date) (* 1000 60 60 24))))

(defn date+day [date]
  (new java.util.Date (+ (.getTime date) (* 1000 60 60 24))))

(defn webdoc-get-url-path [row]
  (str "/doc" (ix/webdoc-get-url-path row)))


(defn price-html [{:keys [price currency]}]
  (list
   [:span {:class "text-warning" :style "font-size:18px"} (int price)]
   (condp = currency
     "0 KZT" " KZT "
     "1 USD" [:span {:class "text-warning glyphicon glyphicon-usd" }]
     "2 EUR" [:span {:class "text-warning glyphicon glyphicon-eur" }]
     " у.е. ")
   ))

(defn content-list-from-text [text]
  (letfn [(clean-str [s] (->> s (apply str) clojure.string/trim))]
    (let [[a, [i _ s]] (reduce
                        (fn [[a,[i t s]] c]
                          (cond (= c \;) [(conj a [(clean-str i),(clean-str s)]), [[] 0 []]]
                                (= c \>) [a, [i 1 s]]
                                (= t 0) [a, [(conj i c) 0 s]]
                                (= t 1) [a, [i 1 (conj s c)]]
                                ))
                        [[],[[] 0 []]]
                        text)]
      (if (empty? i) a
          (conj a [(clean-str i) (clean-str s)] )))))

;;**************************************************************************************************
;;* BEGIN components
;;* tag: <components>
;;*
;;* description: Дополнительные компоненты
;;*
;;**************************************************************************************************

(defn t-table-paginator [id]
  (let [id (cw/xml-id id)]
    [:div.input-group {:id id :role "group" :style "width: 300px"}
     [:div.input-group-btn
      [:button.btn.btn-default {:id (cw/xml-id :btn-goto-first-page id)}
       [:span {:class "glyphicon glyphicon-fast-backward" :aria-hidden "true"} ]]
      [:button.btn.btn-default {:id (cw/xml-id :btn-goto-previus-page id)}
       [:span {:class "glyphicon glyphicon-step-backward" :aria-hidden "true"} ]]]

     [:input.form-control {:id (cw/xml-id :page-num id)  ;;:style "width: 120px"
                           :style "border: 1px solid"
                           :type "number" :min "1" :value "1" :placeholder "Страница"}]

     ;;[:div.btn-group
     ;; [:span {:class "glyphicon glyphicon-th-list" :style "padding: 8px;" :aria-hidden "true"}] " "]

     ;;     [:div.btn-group
     [:div.input-group-btn
      (drop-down {:id (cw/xml-id :select-page-size id) :style "float: left; width: 80px"
                  :class "btn btn-default dropdown-toggle form-control" :onchange ""}
                 (cw/xml-id :select-page-size id) [5 10 15 20 50 100 1000] 5)


      [:button.btn.btn-default {:id (cw/xml-id :btn-goto-next-page id)}
       [:span {:class "glyphicon glyphicon-step-forward" :aria-hidden "true"} ]]]
     ]))

;; END components
;;..................................................................................................


;;**********************************************************************************************


(defn template-main [request

                     {:keys [title
                             description
                             keywords
                             subject
                             abstract
                             author
                             copyright ]
                      :or {title (:anytext (ix/stext-find c/stext-HOME_META_TITLE))
                           description (:anytext (ix/stext-find c/stext-HOME_META_DESCRIPTION))
                           keywords (:anytext (ix/stext-find c/stext-HOME_META_KEYWORDS))
                           subject (:anytext (ix/stext-find c/stext-HOME_META_SUBJECT))
                           abstract (:anytext (ix/stext-find c/stext-HOME_META_ABSTRACT))
                           author (:anytext (ix/stext-find c/stext-META_AUTHOR))
                           copyright (:anytext (ix/stext-find c/stext-META_COPYRIGHT))
                           }
                      :as specific}

                     body]
  (cwb/template-main
   {:title title
    :header-additions
    (list
     (include-css "/css/newvoyage.css")

     [:meta {:charset "UTF-8"}]

     [:meta {:name "author" :content author}]
     [:meta {:name "copyright" :content copyright}]

     [:meta {:name "resource-type" :content "document"}]
     [:meta {:name "document-state" :content "dynamic"}]
     [:meta {:name "robots" :content "All"}]
     [:meta {:name "revizit-after" :content "2 days"}]
     [:meta {:name "revisit" :content "2"}]

     ;; -----------------------------------------------------
     [:meta {:name "description" :content description}]
     [:meta {:name "keywords" :content keywords}]
     [:meta {:name "subject" :content subject}]
     [:meta {:name "abstract" :content abstract}]
     ;; -----------------------------------------------------
     )}
   (list

    (cwb/navbar
     [cwb/navbar-class-default "navbar-fixed-top"]
     (cwb/container
      {}
      (list

       [:div {:class "navbar-header"}
        [:button {:aria-controls "navbar"
                  :aria-expanded "false"
                  :class "navbar-toggle collapsed"
                  :data-target "#navbar-collapse"
                  :data-toggle "collapse"
                  :type "button"}
         [:span {:class "sr-only"} "Toggle navigation"]
         [:span {:class "icon-bar"}]
         [:span {:class "icon-bar"}]
         [:span {:class "icon-bar"}]]
        [:a {:class "navbar-brand" :href "/"}
         [:img {:height 32 :style "position:relative;top:-8px" :alt "Brand" :src "/images/LogoNV64.jpg"}]
         ]
        ]

       (cwb/navbar-collapse
        (cwb/nav
         (list
          (cwb/nav-menu-item "ГЛАВНАЯ" {:href "/"})
          (cwb/nav-menu "СТРАНЫ" {}

                        (for [row (-> ix/webdoc-select*-for-urls
                                      (ix/webdoc-pred-by-tags?-and-childs-tags-of-parent-tag?
                                       [c/tag-place] c/tag-countries)
                                      (korc/order :keyname)
                                      ix/com-exec)]
                          (cwb/nav-menu-item (:keyname row)
                                             {:href (webdoc-get-url-path row)})
                          )
                        )

          (cwb/nav-menu-item "ЦЕНЫ" {:href "/for-price"})
          (cwb/nav-menu-item "СТАТЬИ" {:href "/articles"})
          (cwb/nav-menu-item "ПОДБОР ТУРА" {:href "/tour-selection"})
          (cwb/nav-menu "СЕРВИС" {} (list
                                     (cwb/nav-menu-item "Поиск" {:href "/search"})
                                     (cwb/nav-menu-item "Продажа билетов на автобусы"
                                                        {:href "https://vta.kz:12443/busterminal"})
                                     ))
          (cwb/nav-menu-item "КОНТАКТЫ" {:href "/#contacts"})


          (form-to {:class "navbar-form navbar-right  hidden-xs hidden-sm" :role "search"}
                   [:put "/search"]
                   [:div {:class "form-group"}
                    [:input {:name :fts-query :style "width:80px"
                             :type "text" :class "form-control" :placeholder "Поиск..."}]
                    [:button {:class "btn btn-default" :type "button" :onclick "this.form.submit();"}
                     [:span {:class "glyphicon glyphicon-search"}]
                     ]])



          )))
       ))

     ;;nv-part
     )




    ;;[:div {:style "margin-top:15px"}
    body
    ;; ]

    [:div#modal-error]

    [:input {:type "hidden" :id "username" :value (-> request :session :cemerick.friend/identity :current)}]
    ;;(javascript-tag (str "ixinfestor.page_main.init();"))

    (cwb/container
     {}
     [:div {:class "row" }
      [:div {:class "col-md-12"}
       (:anytext (ix/stext-find :FOOTER))
       ]])

    (cwb/container
     {}
     [:div {:class "row" }
      [:div {:class "col-md-12"}
       (:anytext (ix/stext-find :FOOTER2))
       ]])

    )))





(defn page-main [request]
  (template-main
   request {}

   (list
    (cwb/container-fluid
     {}
     [:div {:class "row"}

      [:div {:class "hidden-xs hidden-sm col-md-12 col-lg-12" :style "text-align:center !important; background-color: #82aed3;"}
       [:div {:style "width:1024px;margin: auto;"}

        [:div {:style "width:671px;float:left;"}
         [:img {:src "images/top/top-left.jpg" :alt "..."}]
         ]

        [:div {:id "top-carousel" :style "width:352px;float:left;"
               :class "carousel slide" :data-ride "carousel"}
         ;; Indicators
         [:ol {:class "carousel-indicators"}
          [:li {:data-target "#top-carousel" :data-slide-to "0" :class "active"}]
          [:li {:data-target "#top-carousel" :data-slide-to "1"}]
          [:li {:data-target "#top-carousel" :data-slide-to "2"}]
          [:li {:data-target "#top-carousel" :data-slide-to "3"}]
          [:li {:data-target "#top-carousel" :data-slide-to "4"}]
          [:li {:data-target "#top-carousel" :data-slide-to "5"}]
          [:li {:data-target "#top-carousel" :data-slide-to "6"}]
          [:li {:data-target "#top-carousel" :data-slide-to "7"}]
          [:li {:data-target "#top-carousel" :data-slide-to "8"}]
          [:li {:data-target "#top-carousel" :data-slide-to "9"}]
          ]

         ;; Wrapper for slides
         [:div {:class "carousel-inner" :role "listbox"}

          [:div {:class "item active"}
           [:img {:src "images/top/top1.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top2.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top3.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top4.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top5.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top6.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top7.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top8.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top9.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]

          [:div {:class "item"}
           [:img {:src "images/top/top10.jpg" :alt "..." :data-holder-rendered "true"}]
           [:div {:class "carousel-caption"} ""]
           ]
          ]

         ;; Controls
         ;; [:a {:class "left carousel-control" :href "#top-carousel" :role "button" :data-slide "prev"}
         ;;  [:span {:class "glyphicon glyphicon-chevron-left" :aria-hidden "true"}]
         ;;  [:span {:class "sr-only"} "Previous" ]
         ;;  ]
         [:a {:class "right carousel-control" :href "#top-carousel" :role "button" :data-slide "next"}
          [:span {:class "glyphicon glyphicon-chevron-right" :aria-hidden "true"}]
          [:span {:class "sr-only"} "Next" ]
          ]
         ]
        ]
       ]
      ]
     )

    (cwb/container
     {}
     (list

      ;; [:ol {:class "breadcrumb"}
      ;;  [:li [:a {:href="#"} "Home" ]]
      ;;  [:li {:class "active"} "Data" ]
      ;;  ]

      [:div {:class "row" }

       [:div {:class "col-xs-12 col-sm-10 col-md-10"}

        [:div {:id "welcome" :class "row" }
         [:a {:name "welcome"}]
         (let [i  (-> :PHOTO-OF-THE-WEEK ix/stext-find :anytext)]
           (when (not (empty? i))
             (let [c1 (-> :PHOTO-OF-THE-WEEK-COLOR-1 ix/stext-find :anytext)
                   c2 (-> :PHOTO-OF-THE-WEEK-COLOR-2 ix/stext-find :anytext)] ;;"/image/2015/07/15/74820/IMG_4929_1.JPG"
               [:a {:href i :target "_blank"  :style  "float: right;"}
                [:div {:style "position:relative;"}
                 [:img {:alt "Фото недели" :src i
                        :style "height:110px;margin: 20px 10px;"}]
                 [:h4 {:style (str "position:absolute;z-index:10;top:84px;left:20px;color:" c1 ";text-shadow: 0px 0px 4px " c2 ",0px 0px 7px " c2 "" )}
                  "Фото недели"]
                 ]])))
         (:anytext (ix/stext-find :GOLD_FIELD))
         ]

        [:div {:id "adverts-special" :class "row"}
         [:a {:name "adverts-special"}]
         (cwb/page-header [:h3 "Спецпредложения"])

         (for [row (-> ix/webdoc-select*
                       (korc/fields :id :keyname :ttitle :web_title_image :web_top_description
                                    :cdate :price :currency)
                       (korc/with ix/tag)
                       (ix/webdoc-pred-by-tags? [c/tag-advert c/tag-special-offer])
                       (korc/order :orderindex)
                       (korc/where (= :showing true))
                       (korc/where (and (< :showbdate (new java.sql.Timestamp (.getTime (date+day (new java.util.Date)))))
                                        (> :showedate (new java.sql.Timestamp (.getTime (date-day (new java.util.Date)))))))
                       (ix/com-exec))]
           [:a {:href (webdoc-get-url-path row)}
            [:div {:class "adverts-table col-sm-6 col-md-4"}
             [:div {:class "thumbnail adverts" :style "height: 180px"}
              [:img {:class "img-circle adverts-img"
                     :src (row :web_title_image) :alt "Аватарка"}]


              (when (ix/webdoc-row-contain-tag? row c/tag-special-offer-fire)
                [:span {:class "text-warning glyphicon glyphicon-fire" :style "float:right;font-size:1.5em"}])

              (when (ix/webdoc-row-contain-tag? row c/tag-special-offer-plane)
                [:span {:class "text-primary glyphicon glyphicon-plane" :style "float:right;font-size:1.5em"}])

              [:span {:class "text-primary" :style "line-height:60%;font-size:15px"} (row :keyname)]
              [:br]
              (price-html row)

              [:div {:class "caption"}
               [:p (row :web_top_description)]
               ]]]]
           )
         ]

        ;; [:div {:id "adverts-standart" :class "row"}
        ;;  [:a {:name "adverts-standart"}]
        ;;  (cwb/page-header [:h3 "Объявления"])

        ;;  (for [row (-> ix/webdoc-select*
        ;;                (korc/fields :id :keyname :ttitle :web_title_image :web_top_description :cdate :price)
        ;;                (ix/webdoc-pred-by-tags? [c/tag-advert c/tag-standart-offer])
        ;;                (korc/order :orderindex)
        ;;                ;;(korc/where (= :showing true))
        ;;                (ix/com-exec))]
        ;;    [:a {:href (webdoc-get-url-path row)}
        ;;     [:div {:class "col-sm-6 col-md-4"}
        ;;      [:div {:class "thumbnail" :style "height: 180px"}
        ;;       [:img {:class "img-circle adverts-img"
        ;;
        ;;              :src (row :web_title_image) :alt "Аватарка"}]
        ;;       [:span {:class "text-primary" :style "line-height:60%;font-size:18px"} (row :keyname)]
        ;;       [:br]
        ;;       [:span {:class "text-warning" :style "font-size:20px"} (row :price)]
        ;;       [:div {:class "caption"}
        ;;        [:p (row :web_top_description)]
        ;;        ]]]]
        ;;    )
        ;;  ]

        [:div {:id "news" :class "row" }
         [:a {:name "news"}]
         (cwb/page-header [:h3 "Новости"])

         (let [{{:keys [news-page] :or {news-page "1"}}:params} request
               page (-> news-page Long/parseLong)
               page-size (-> ix/webdoc-select*
                             (ix/webdoc-pred-by-tags? [c/tag-news])
                             ix/com-count
                             (/ 10)
                             int)
               part 5
               ibegin  (if (< (- page part) 1) 1 (- page part))
               iend    (+ 2 (if (> (+ page part) page-size) page-size (+ page part)))

               ]

           (list
            (for [row (-> ix/webdoc-select*-for-urls
                          (ix/webdoc-pred-by-tags? [c/tag-news])
                          (korc/order :cdate :desc)
                          (korc/offset (* (dec page) 10))
                          (korc/limit 10)
                          (ix/com-exec))]

              [:a {:class "a-news" :href (webdoc-get-url-path row)}
               [:div {:class "panel panel-default news"}

                [:div {:class "media panel-body"}
                 [:div {:class "media-left"}
                  (when-let [i (row :web_title_image)]
                    [:img {:class "media-object"
                           :style "width:128px"
                           :src i :alt "Аватарка"}])
                  ]
                 [:div {:class "media-body"}
                  [:h4 {:class "media-heading"}
                   [:small ;;{:style "font-size:15px"}
                    (.format news-date-format (row :cdate))]
                   [:br]
                   (row :keyname)]
                  (row :web_top_description)
                  ]]]]
              )

            [:nav
             [:ul {:class "pagination"}
              [:li {:class ""} [:a {:href (str "/?news-page=1#news") :aria-label "Previous"}
                                [:span {:aria-hidden "true"} "&laquo;"]]]

              (->> (range ibegin iend)
                   (map (fn [i]
                          [:li {:class (if (= page i) "active" "")}
                           [:a {:href (str "/?news-page=" i "#news")} i
                            [:span {:class "sr-only"}]]])))

              [:li {:class ""} [:a {:href (str "/?news-page=" (inc page-size) "#news") :aria-label "Previous"}
                                [:span {:aria-hidden "true"} "&raquo;"]]]
              ]
             ]
            ))

         ]

        [:div {:id "company" :class "row"}
         [:a {:name "company"}]
         (cwb/page-header [:h3 "О компании"])
         (:anytext (ix/stext-find :ABOUT))
         ]

        [:div {:id "contacts" :class "row"}
         [:a {:name "contacts"}]
         (cwb/page-header [:h3 "Как с нами связаться"])
         (:anytext (ix/stext-find c/stext-contacts-key))
         (let [{{:keys [name phone email wishes] :as params} :params} request]
           (list
            [:a {:name "fast-message-form"}]
            (when (not (empty? email))
              (let [{:keys [code error message]} (postal/send-message
                                                  {:host "smtp.gmail.com"
                                                   :user "politrendkz"
                                                   :pass "paradox_kz"
                                                   :ssl :yes!!!11}
                                                  {:from "site@vta.kz"
                                                   :to ["t34box@gmail.com" "politrend.web@gmail.com" "517102@mail.ru"]
                                                   :cc email
                                                   :subject "Сообщение с сайта"
                                                   :body [{:type "text/html;charset=utf-8"
                                                           :content (html5
                                                                     [:table {:border "1"}
                                                                      [:thead
                                                                       [:tr [:td [:h4 "Параметр"]] [:td [:h4 "Значение"]]]]
                                                                      [:tbody
                                                                       [:tr [:td "\n Имя отправителя:\t"] [:td name]]
                                                                       [:tr [:td "\n Телефон отправителя:\t"] [:td phone]]
                                                                       [:tr [:td "\n Електронный адрес:\t"] [:td email]]
                                                                       [:tr [:td "\n пожелания:\n\t"] [:td wishes]]
                                                                       ]])
                                                           }]})]
                (if (= code 0)
                  [:div {:class "alert alert-success" :role "alert"} "Ваше сообщение успешно отправлено"]
                  [:div {:class "alert alert-danger" :role "alert"} (str "Ваше сообщение не было отправлено:" message)])
                ))
            (form-to {:class "form-horizontal col-sm-12 col-md-12 col-lg-12 news"
                      :style "background-color: white;"}
                     [:post "/#fast-message-form"]
                     [:fieldset
                      [:legend
                       [:span {:class "glyphicon glyphicon-pencil"
                               :style "font-size:1em" :aria-hidden "true"}]
                       " Написать сообщение прямо сейчас "
                       ]

                      [:div {:class "form-group"}
                       [:label {:class "col-sm-3 control-label" :for "name"} "Имя"]
                       [:div {:class "col-sm-9"}
                        [:input {:id "name" :name "name"
                                 :class "form-control" :required true
                                 :placeholder "Укажите ваше имя...", :type "text"
                                 :value name}]]]

                      [:div {:class "form-group"}
                       [:label {:class "col-sm-3 control-label" :for "email"} "Почта"]
                       [:div {:class "col-sm-9"}
                        [:input {:id "email" :name "email"
                                 :class "form-control" :required true
                                 :placeholder "Укажите адрес электронной почты...", :type "email"
                                 :value email}]]]

                      [:div {:class "form-group"}
                       [:label {:class "col-sm-3 control-label" :for "phone"} "Телефон"]
                       [:div {:class "col-sm-9"}
                        [:input {:id "phone" :name "phone"
                                 :class "form-control" :required true
                                 :placeholder "Укажите номер вашего телефона...", :type "tel"
                                 :value phone}]]]


                      [:div {:class "form-group"}
                       [:label {:class "col-sm-3 control-label" :for "wishes"} "Пожелания по отдыху"]
                       [:div {:class "col-sm-9"}
                        [:textarea {:id "wishes" :name "wishes"
                                    :class "form-control" :rows "4"
                                    :placeholder "название отеля, звездность и другие пожелания по отдыху", :type "text"}
                         wishes]]]

                      [:button {:type "submit" :class "btn btn-primary col-sm-offset-9 col-md-offset-9 col-lg-offset-9 col-sm-3 col-md-3 col-lg-3"} "Отправить запрос "
                       [:span {:class "glyphicon glyphicon-send"
                               :style "font-size:1em" :aria-hidden "true"}]]
                      [:br]
                      [:br]
                      ])))
         ]

        ]

       [:div {:id :body-scroll-spy :class "hidden-xs col-sm-2 col-md-2"}

        [:ul {:class "nav nav-pills nav-stacked"
              :data-spy "affix" :data-offset-top "80" :data-offset-bottom "0"}
         [:li [:a {:href "#welcome"} "Приветствие"]]
         [:li [:a {:href "#adverts-special"} "Спецпредложения"]]
         ;;[:li [:a {:href "#adverts-standart"} "Объявления"]]
         [:li [:a {:href "#news"} "Новости"]]
         [:li [:a {:href "#company"} "О компании"]]
         [:li [:a {:href "#contacts"} "Контакты"]]
         ]
        ]
       ]
      )))))



(defn page-articles [request]
  (template-main
   request {:title "Статьи"}
   (cwb/container
    {}

    [:div {:class "row" }

     [:div {:class "col-md-12"}

      [:ol {:class "breadcrumb"}
       [:li [:a {:href "/"} "Главная" ]]
       [:li {:class "active"} "Статьи" ]
       ]


      (cwb/page-header [:h1 "Статьи"])


      (for [row (-> ix/webdoc-select*-for-urls
                    (ix/webdoc-pred-by-tags? [c/tag-article])
                    (korc/order :cdate :desc)
                    (ix/com-exec))]

        [:a {:class "a-news" :href (webdoc-get-url-path row)}
         [:div {:class "panel panel-default news"}
          [:div {:class "media panel-body"}
           [:div {:class "media-left"}
            (when-let [i (row :web_title_image)]
              [:img {:class "media-object"
                     :style "width:128px"
                     :src i :alt "Аватарка"}])
            ]
           [:div {:class "media-body"}
            [:h4 {:class "media-heading"}
             [:small ;;{:style "font-size:15px"}
              (.format news-date-format (row :cdate))]
             [:br]
             (row :keyname)]
            (row :web_top_description)
            ]]]]
        )

      ]

     ]
    )))


(defn page-for-price [request]
  (template-main
   request {:title "Все ценовые предложения"}
   (cwb/container
    {}

    [:div {:class "row" }

     [:div {:class "col-md-12"}

      [:ol {:class "breadcrumb"}
       [:li [:a {:href "/"} "Главная" ]]
       [:li {:class "active"} "Цены" ]
       ]


      (cwb/page-header [:h1 "Цены"])

      (for [row (-> ix/webdoc-select*
                    (korc/fields :id :keyname :ttitle :web_title_image :web_top_description
                                 :cdate :price :currency)
                    (ix/webdoc-pred-by-tags? [c/tag-advert])
                    (korc/order [:currency :price])
                    (korc/with ix/tag)
                    ;;(korc/order :orderindex)
                    (korc/where (= :showing true))
                    (korc/where (and (< :showbdate (new java.sql.Timestamp (.getTime (date+day (new java.util.Date)))))
                                     (> :showedate (new java.sql.Timestamp (.getTime (date-day (new java.util.Date)))))))
                    (ix/com-exec))]
        [:a {:href (webdoc-get-url-path row)}
         [:div {:class "adverts-table col-sm-6 col-md-4"}
          [:div {:class "thumbnail adverts" :style "height: 180px"}
           [:img {:class "img-circle adverts-img"

                  :src (row :web_title_image) :alt "Аватарка"}]

           (when (ix/webdoc-row-contain-tag? row c/tag-special-offer-fire)
             [:span {:class "text-warning glyphicon glyphicon-fire" :style "float:right;font-size:1.5em"}])

           (when (ix/webdoc-row-contain-tag? row c/tag-special-offer-plane)
             [:span {:class "text-primary glyphicon glyphicon-plane" :style "float:right;font-size:1.5em"}])

           [:span {:class "text-primary" :style "line-height:60%;font-size:15px"} (row :keyname)]
           [:br]
           (price-html row)
           [:div {:class "caption"}
            [:p (row :web_top_description)]
            ]]]]
        )


      ]

     ]
    )))



;; [:ol {:class "breadcrumb"}
;;  [:li [:a {:href="#"} "Home" ]]
;;  [:li {:class "active"} "Data" ]
;;  ]


(defn page-tag [request]
  (let [{:keys [id tagname description]
         :as tag-row} (-> ix/tag-list*
                          (korc/where (= :id (-> request :params :id Long/parseLong)))
                          ix/com-exec-1)]
    (template-main
     request {}
     (cwb/container
      {}

      [:div {:class "row" }

       [:div {:class "col-md-12"}

        [:ol {:class "breadcrumb"}
         [:li "теги"]
         (let [[[tag-row _] & t] (ix/tag-get-path-and-join-webdoc-for-urls [c/tag-place] tag-row)]
           (reduce
            (fn [a [tag-row webdoc-url-row]]
              (conj a
                    (if webdoc-url-row
                      (let [{:keys [id keyname]} webdoc-url-row]
                        [:li [:a {:href (str "/doc/" id "/" keyname)} keyname ]])
                      (let [{:keys [id tagname]} tag-row]
                        [:li [:a {:href (str "/taginfo/" id "/" tagname)} tagname ]]))))
            (list [:li {:class "active"} (:tagname tag-row)]) t))
         ]


        (cwb/page-header [:h1 "Тег " [:span {:class "text-primary"} tagname]])

        (for [[t w] (ix/tag-get-tree-childs-and-join-webdoc-for-urls
                     [c/tag-place] tag-row)]
          (if w
            [:a {:style "padding:5px" :href (webdoc-get-url-path w)} (:keyname w)]
            [:a {:style "padding:5px" :href (str "/taginfo/" (:id t))} (:tagname t)]
            ))


        [:p description]
        ]

       ]
      ))))

(def rating-map
  (->> [c/tag-hotel-rating-1
        c/tag-hotel-rating-2
        c/tag-hotel-rating-3
        c/tag-hotel-rating-4
        c/tag-hotel-rating-5]
       (map #(vector (:id %2) (inc %1)) (range))
       (reduce (fn [a [t r]] (assoc a t r)) {})
       ))

(defn webdoc-get-hotel-max-rating [{:keys [tag]}]
  (->> tag
       (map (comp rating-map :id))
       (filter (comp not nil?))
       (#(if (empty? %) 0 (apply max %)))))


(defn page-doc [request]
  (let [{:keys [id cdate keyname tag web_top_description web_description
                web_meta_keywords web_meta_description web_meta_subject]
         :as webdoc-row} (-> c/webdoc-select*
                             (korc/where (= :id (-> request :params :id Long/parseLong)))
                             (korc/with ix/tag)
                             ix/com-exec-1)

         images (-> webdoc-row
                    (ix/files_rel-select-files-by-* :webdoc_id)
                    ix/file-pred-images*
                    ix/file-pred-galleria?
                    ix/com-exec)
         images? (not (empty? images))

         files (-> webdoc-row
                   (ix/files_rel-select-files-by-* :webdoc_id)
                   (ix/file-pred-images* :not-image)
                   ix/com-exec)
         files? (not (empty? files))


         is-place? (ix/webdoc-row-contain-tag? webdoc-row c/tag-place)
         is-hotel? (ix/webdoc-row-contain-tag? webdoc-row c/tag-hotel)

         place-tags-path (when (or is-place? is-hotel?)
                           (ix/webdoc-row-get-tags-paths-to-root-parent-and-join-webdoc-for-urls
                            webdoc-row [c/tag-place] c/tag-countries))

         [[this-tag-point _] & t] place-tags-path


         special-offers (when is-place?
                          (-> ix/webdoc-select*
                              (korc/fields :id :keyname :ttitle :web_title_image :web_top_description
                                           :cdate :price :currency)
                              (korc/with ix/tag)
                              (ix/webdoc-pred-by-tags? [c/tag-advert c/tag-special-offer])
                              (ix/webdoc-pred-search-for-the-child-tree-tags? this-tag-point)
                              (korc/order :orderindex)
                              (korc/where (= :showing true))
                              (korc/where (and (< :showbdate (new java.sql.Timestamp (.getTime (date+day (new java.util.Date)))))
                                               (> :showedate (new java.sql.Timestamp (.getTime (date-day (new java.util.Date)))))))
                              (ix/com-exec)))
         special-offers? (not (empty? special-offers))

         standart-offers (when is-place?
                           (-> ix/webdoc-select*
                               (korc/fields :id :keyname :ttitle :web_title_image :web_top_description
                                            :cdate :price :currency)
                               (korc/with ix/tag)
                               (ix/webdoc-pred-by-tags? [c/tag-advert c/tag-standart-offer])
                               (ix/webdoc-pred-search-for-the-child-tree-tags? this-tag-point)
                               (korc/order :orderindex)
                               (korc/where (= :showing true))
                               (korc/where (and (< :showbdate (new java.sql.Timestamp (.getTime (date+day (new java.util.Date)))))
                                                (> :showedate (new java.sql.Timestamp (.getTime (date-day (new java.util.Date)))))))
                               (ix/com-exec)))

         standart-offers? (not (empty? standart-offers))

         hotels (when is-place?
                  (-> ix/webdoc-select*
                      (korc/fields :id :keyname :ttitle :web_title_image :web_top_description)
                      (korc/with ix/tag)
                      (ix/webdoc-pred-by-tags? [c/tag-hotel])
                      (ix/webdoc-pred-search-for-the-child-tree-tags? this-tag-point)
                      (ix/com-exec)))

         hotels? (not (empty? hotels))

         ]
    (template-main
     request {:title keyname
              :description web_meta_description
              :keywords web_meta_keywords
              :subject web_meta_subject}
     (cwb/container
      {}

      [:div {:class "row" }

       [:div {:class "col-xs-12 col-sm-10 col-md-10"}

        (when (or is-place? is-hotel?)
          [:div {:class "row" }
           [:ol {:class "breadcrumb"}
            (reduce
             (fn [a [tag-row webdoc-url-row]]
               (conj a
                     (if webdoc-url-row
                       (let [{:keys [id keyname]} webdoc-url-row]
                         [:li [:a {:href (str "/doc/" id "/" keyname)} keyname ]])
                       (let [{:keys [id tagname]} tag-row]
                         [:li [:a {:href (str "/taginfo/" id "/" tagname)} tagname ]]))))
             (list [:li {:class "active"} (:tagname this-tag-point)]) (rest place-tags-path))
            ]])

        (when is-place?
          [:div {:id "top" :class "row" }
           [:a {:name "top"}]
           (cwb/page-header
            (list [:h1 keyname]

                  (for [[t w] (ix/tag-get-tree-childs-and-join-webdoc-for-urls
                               [c/tag-place] (ffirst place-tags-path))]
                    (if w
                      [:a {:style "padding:5px" :href (webdoc-get-url-path w)} (:keyname w)]
                      [:a {:style "padding:5px" :href (str "/taginfo/" (:id t))} (:tagname t)]
                      ))
                  ))
           web_top_description])

        ;; ADVERTS ----------------------------------------------------------------------------------------------

        (when special-offers?
          [:div {:id "adverts-special" :class "row"}
           [:a {:name "adverts-special"}]
           (cwb/page-header [:h3 "Спецпредложения"])

           (for [row special-offers]
             [:a {:href (webdoc-get-url-path row)}
              [:div {:class "adverts-table col-sm-6 col-md-4"}
               [:div {:class "thumbnail adverts" :style "height: 180px"}
                [:img {:class "img-circle adverts-img"

                       :src (row :web_title_image) :alt "Аватарка"}]

                (when (ix/webdoc-row-contain-tag? row c/tag-special-offer-fire)
                  [:span {:class "text-warning glyphicon glyphicon-fire" :style "float:right;font-size:1.5em"}])

                (when (ix/webdoc-row-contain-tag? row c/tag-special-offer-plane)
                  [:span {:class "text-primary glyphicon glyphicon-plane" :style "float:right;font-size:1.5em"}])

                [:span {:class "text-primary" :style "line-height:60%;font-size:15px"} (row :keyname)]
                [:br]
                (price-html row)
                [:div {:class "caption"}
                 [:p (row :web_top_description)]
                 ]]]]
             )
           ])

        (when standart-offers?
          [:div {:id "adverts-standart" :class "row"}
           [:a {:name "adverts-standart"}]
           (cwb/page-header [:h3 "Объявления"])

           (for [row standart-offers]
             [:a {:href (webdoc-get-url-path row)}
              [:div {:class "adverts-table col-sm-6 col-md-4"}
               [:div {:class "thumbnail adverts" :style "height: 180px"}
                [:img {:class "img-circle adverts-img"

                       :src (row :web_title_image) :alt "Аватарка"}]
                [:span {:class "text-primary" :style "line-height:60%;font-size:18px"} (row :keyname)]
                [:br]
                (price-html row)
                [:div {:class "caption"}
                 [:p (row :web_top_description)]
                 ]]]]
             )
           ])

        ;; Описание ---------------------------------------------------------------------------------

        [:div {:id "web_description" :class "row" }
         [:a {:name "web_description"}]
         (cwb/page-header
          (list [:h2 (if is-place? "Описание" keyname)
                 (when (ix/webdoc-row-contain-tag? webdoc-row c/tag-hotel)
                   (->> webdoc-row
                        webdoc-get-hotel-max-rating
                        (range 0)
                        (map (fn [_]
                               [:span {:class "glyphicon glyphicon-star text-warning"
                                       :style "font-size:1em;float:right" :aria-hidden "true"}]))
                        ))]
                (when (ix/webdoc-row-contain-tag? webdoc-row c/tag-news)
                  [:small "Дата: " (.format news-date-format cdate)])
                ))
         web_description]

        ;; файлы ------------------------------------------------------------------------------------

        (when files?
          [:div {:id "files" :class "row" }
           [:a {:name "files"}]
           (cwb/page-header [:h2 "Файлы"])
           (for [i files]
             (let [file-s (str "/file/" (:path i))]
               [:div
                [:blockquote
                 [:h5 (:top_description i)]
                 (:description i)
                 [:br]
                 [:a {:class "btn btn-success" :href file-s :target "_blank"} "Скачать"]
                 " "[:b (:filename i)]]
                ]
               ))
           ])

        ;; Картинки ---------------------------------------------------------------------------------
        (when images?
          [:div {:id "gallery" :class "row" }
           [:a {:name "gallery"}]
           (cwb/page-header [:h2 "Галерея"])
           (list
            (include-css "/css/blueimp-gallery.min.css")
            (include-css "/css/blueimp-gallery.setup.css")

            [:div {:id "blueimp-gallery" :class "blueimp-gallery"}
             [:div {:class "slides"}]
             [:h3  {:class "title"}]
             [:p {:class "description"}]
             [:a {:class "prev"}"‹"]
             [:a {:class "next"}"›"]
             [:a {:class "close"}"×"]
             [:a {:class "play-pause"}]
             [:ol {:class "indicator"}]
             ]

            [:div {:id "links" :class "row"}
             (for [{:keys [path top_description description]} images]
               [:div {:class "col-xs-4 col-sm-3 col-md-2"}
                [:div {:class "thumbnail"}
                 [:a {:href path :title top_description :data-description description}
                  [:img {:src path :alt top_description :style "height: 64px"}]]]]
               )
             ]

            (include-js "/js/jquery.blueimp-gallery.min.js")
            (include-js "/js/jquery.blueimp-gallery.setup.js"))
           ])

        (when hotels?
          [:div {:id "hotels" :class "row" }
           [:a {:name "hotels"}]
           (cwb/page-header [:h2 "Отели"])
           (for [row hotels]
             [:a {:class "a-news" :href (webdoc-get-url-path row)}
              [:div {:class "panel panel-default news"}

               [:div {:class "media panel-body"}
                [:div {:class "media-left"}
                 (when-let [i (row :web_title_image)]
                   [:img {:class "media-object"
                          :style "width:64px"
                          :src i :alt "Аватарка"}])
                 ]
                [:div {:class "media-body"}
                 [:h4 {:class "media-heading"}
                  (->> row
                       webdoc-get-hotel-max-rating
                       (range 0)
                       (map (fn [_]
                              [:span {:class "glyphicon glyphicon-star text-warning"
                                      :style "font-size:1em;float:left" :aria-hidden "true"}]))
                       )

                  [:br]
                  (row :keyname)]
                 (row :web_top_description)
                 ]]]]
             )])



        ]


       ;; Меню навигации по странице ----------------------------------------------------------------
       [:div {:id :body-scroll-spy
              :class "hidden-xs col-sm-2 col-md-2"
              :style "padding-top: 50px"}

        [:ul {:class "nav nav-pills nav-stacked" :data-spy "affix"
              :data-offset-top "100" :data-offset-bottom "30"}

         (when special-offers?
           [:li [:a {:href "#adverts-special"} "Спецпредложения"]])
         (when standart-offers?
           [:li [:a {:href "#adverts-standart"} "Объявления"]])

         [:li [:a {:href "#web_description"} "Описание"]]

         (when-let [citems (webdoc-row :web_citems)]
           (->> citems
                content-list-from-text
                (filter #(not (nil? %)))
                (map (fn [[href caption is-active]]
                       [:li [:a {:href href } caption]]
                       ))))


         (when files?
           [:li [:a {:href "#files"} "Файлы"]])

         (when images?
           [:li [:a {:href "#gallery"} "Галерея"]])

         (when hotels?
           [:li [:a {:href "#hotels"} "Отели"]])

         ]
        ]
       ]
      ))))




(defn page-search [request]
  (let [{{:keys [page fts-query] :or {page "1" fts-query ""}}:params} request
        page (-> page Long/parseLong)


        query* (-> ix/webdoc-select*
                   (as-> query
                       (let [fts-query (clojure.string/trim fts-query)]
                         (if (empty? fts-query)
                           query
                           (ix/webdoc-pred-search? query fts-query)))))


        page-size (-> query*
                      ix/com-count
                      (/ 10)
                      int)
        part 5
        ibegin  (if (< (- page part) 1) 1 (- page part))
        iend    (+ 2 (if (> (+ page part) page-size) page-size (+ page part)))
        ]

    (template-main
     request {}
     (cwb/container
      {}

      [:div {:class "row" }

       [:div {:class "col-md-12"}

        [:ol {:class "breadcrumb"}
         [:li [:a {:href "/"} "Главная" ]]
         [:li {:class "active"} "поиск" ]
         ]

        [:div {:id "form" :class "row"}

         (cwb/page-header
          (form-to {:class "form-horizontal col-sm-12 col-md-12 col-lg-12 news"
                    :style "background-color: #fff;"}
                   [:get "/search"]

                   [:fieldset
                    [:legend
                     [:span {:class "glyphicon glyphicon-search"
                             :style "font-size:1em" :aria-hidden "true"}]
                     " Поисковая форма"
                     ]

                    [:div {:class "input-group"}
                     [:input {:name :fts-query
                              :type "text" :class "form-control" :placeholder "Поиск..."
                              :value fts-query}]
                     [:span {:class "input-group-btn"}
                      [:button {:class "btn btn-default" :type "button" :onclick "this.form.submit();"}
                       [:span {:class "glyphicon glyphicon-search"}]]
                      ]]


                    [:br]]

                   ))
         ]

        [:br]
        [:hr]

        [:div {:id "result" :class "row"}


         (for [{:keys [id keyname web_description web_title_image
                       web_top_description cdate tag]
                :as row} (-> query*
                             (korc/fields :id :keyname :web_description :web_title_image
                                          :web_top_description :cdate)
                             (korc/with ix/tag)
                             (korc/offset (* (dec page) 10))
                             (korc/limit 10)
                             (ix/com-exec))]

           [:a {:class "a-news" :href (webdoc-get-url-path row)}
            [:div {:class "panel panel-default news"}

             [:div {:class "media panel-body"}
              [:div {:class "media-left"}
               (when-let [i web_title_image]
                 [:img {:class "media-object"
                        :style "width:128px"
                        :src i :alt "Аватарка"}])
               ]
              [:div {:class "media-body"}
               [:h4 {:class "media-heading"}
                [:small (.format news-date-format cdate)]
                [:small (->> row
                             webdoc-get-hotel-max-rating
                             (range 0)
                             (map (fn [_]
                                    [:span {:class "glyphicon glyphicon-star text-warning"
                                            :style "font-size:1em;float:right" :aria-hidden "true"}]))
                             )]

                [:br]
                keyname]
               web_top_description
               [:div
                (map (fn [{tagname :tagname}]
                       [:span {:class "label label-primary" :style "margin-right:5px"} tagname])
                     tag)
                ]
               ]]]]
           )

         [:nav
          [:ul {:class "pagination"}
           [:li {:class ""} [:a {:href (str "/search?page=1&fts-query=" fts-query) :aria-label "Previous"}
                             [:span {:aria-hidden "true"} "&laquo;"]]]

           (->> (range ibegin iend)
                (map (fn [i]
                       [:li {:class (if (= page i) "active" "")}
                        [:a {:href (str "/search?page=" i "&fts-query=" fts-query)} i
                         [:span {:class "sr-only"}]]])))

           [:li {:class ""} [:a {:href (str "/search?page=" (inc page-size) "&fts-query=" fts-query) :aria-label "Previous"}
                             [:span {:aria-hidden "true"} "&raquo;"]]]
           ]
          ]

         ]
        ]
       ]
      ))))


(defn page-tour-selection [{{:keys [country
                                    region
                                    city
                                    date
                                    days
                                    primates
                                    adults
                                    childs-2-12
                                    childs-2
                                    hotel
                                    rating-1 rating-2
                                    coastline


                                    name
                                    phone
                                    email

                                    wishes
                                    ] :as params}:params
                                      :as request}]

  ;;(println "\nMAIL:\n" result "\n\n")
  ;;Результат: {:code 0, :error :SUCCESS, :message messages sent}

  (template-main
   request {}
   (cwb/container
    {}

    [:div {:class "row" }

     [:div {:class "col-md-12"}

      [:ol {:class "breadcrumb"}
       [:li [:a {:href "/"} "Главная" ]]
       [:li {:class "active"} "Подбор тура" ]
       ]


      (when (not (empty? email))
        (let [{:keys [code error message]} (postal/send-message
                                            {:host "smtp.gmail.com"
                                             :user "politrendkz"
                                             :pass "paradox_kz"
                                             :ssl :yes!!!11}
                                            {:from "site@vta.kz"
                                             :to ["t34box@gmail.com" "politrend.web@gmail.com" "517102@mail.ru"]
                                             :cc email
                                             :subject "Сообщение на запрос тура"
                                             :body [{:type "text/html;charset=utf-8"
                                                     :content (html5
                                                               [:table {:border "1"}
                                                                [:thead
                                                                 [:tr [:td [:h4 "Параметр"]] [:td [:h4 "Значение"]]]]
                                                                [:tbody
                                                                 [:tr [:td "\n Страна:\t"] [:td country]]
                                                                 ;;[:tr [:td "\n Регион:\t"] [:td region]]
                                                                 ;;[:tr [:td "\n Город:\t"] [:td city]]
                                                                 [:tr [:td "\n Дата отъезда:\t"] [:td date]]
                                                                 [:tr [:td "\n Количество дней отдыха:\t"] [:td days]]
                                                                 ;;[:tr [:td "\n Количество человек:\t"] [:td primates]]
                                                                 [:tr [:td "\n взрослые (от 12 лет):\t"] [:td adults]]
                                                                 [:tr [:td "\n из них дети (от 2 до 12 лет):\t"] [:td childs-2-12]]
                                                                 [:tr [:td "\n из них дети (до 2 лет):\t"] [:td childs-2]]
                                                                 [:tr [:td "\n пожелания:\n\t"] [:td wishes]]
                                                                 ;;[:tr [:td "\n Отель:\t"] [:td hotel]]
                                                                 ;;[:tr [:td "\n Рейтинг отеля:\t от " rating-1 " до " rating-2
                                                                 ;;[:tr [:td "\n Береговая линия:\t"] [:td coastline]]
                                                                 [:tr [:td "\n Имя отправителя:\t"] [:td name]]
                                                                 [:tr [:td "\n Телефон отправителя:\t"] [:td phone]]
                                                                 [:tr [:td "\n Електронный адрес:\t"] [:td email]]
                                                                 ]])
                                                     }]})]
          (if (= code 0)
            [:div {:class "alert alert-success" :role "alert"} "Ваше сообщение успешно отправлено"]
            [:div {:class "alert alert-danger" :role "alert"} (str "Ваше сообщение не было отправлено:" message)])
          ))



      (form-to {:class "form-horizontal col-sm-12 col-md-12 col-lg-12 news"
                :style "background-color: white;"}
               [:post "/tour-selection"]
               [:fieldset
                [:legend
                 [:span {:class "glyphicon glyphicon-pencil"
                         :style "font-size:1em" :aria-hidden "true"}]
                 " Форма запроса на подбора тура "
                 ]


                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "country"} "Страна"]
                 [:div {:class "col-sm-9"}
                  [:input {:id "country" :name "country"
                           :class "form-control"
                           :placeholder "Куда бы вы хотели поехать...", :type "text"
                           :value country}]]]


                ;; [:div {:class "form-group"}
                ;;  [:label {:class "col-sm-3 control-label" :for "region"} "Регион"]
                ;;  [:div {:class "col-sm-9"}
                ;;   [:input {:id "region" :name "region"
                ;;            :class "form-control"
                ;;            :placeholder "Регион или область...", :type "text"
                ;;            :value country}]]]

                ;; [:div {:class "form-group"}
                ;;  [:label {:class "col-sm-3 control-label" :for "city"} "Населенный пункт"]
                ;;  [:div {:class "col-sm-9"}
                ;;   [:input {:id "city" :name "city"
                ;;            :class "form-control"
                ;;            :placeholder "Город, поселок или иное место...", :type "text"
                ;;            :value country}]]]


                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "date"} "Дата отъезда"]
                 [:div {:class "col-sm-12 col-md-5 col-lg-4"}
                  [:input {:id "date" :name "date"
                           :class "form-control"
                           :type "text"
                           :placeholder "дата в свободной форме"
                           ;;:type "date"
                           ;;:pattern "\\d{4}-\\d{2}-\\d{2}"
                           ;;:placeholder "yyyy-MM-dd"
                           :value date}]]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "days"} "Количество дней отдыха"]
                 [:div {:class "col-sm-12 col-md-4 col-lg-3"}
                  (drop-down {:class "form-control"} :days
                             ["1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "14 и более..."]
                             days)]]

                ;; [:div {:class "form-group"}
                ;;  [:label {:class "col-sm-3 control-label" :for "primates"} "Количество человек"]
                ;;  [:div {:class "col-sm-12 col-md-4 col-lg-3"}
                ;;   (drop-down {:class "form-control"} :primates
                ;;              ["1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "14 и более..."]
                ;;              primates)]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "adults"} "Взрослых (от 12 лет)"]
                 [:div {:class "col-sm-12 col-md-4 col-lg-3"}
                  (drop-down {:class "form-control"} :adults
                             ["1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "14 и более..."]
                             adults)]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "childs-2-12"} "из них дети (от 2 до 12 лет):"]
                 [:div {:class "col-sm-12 col-md-4 col-lg-3"}
                  (drop-down {:class "form-control"} :childs-2-12
                             ["нет" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "14 и более..."]
                             childs-2-12)]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "childs-2"} "из них дети (до 2 лет):"]
                 [:div {:class "col-sm-12 col-md-4 col-lg-3"}
                  (drop-down {:class "form-control"} :childs-2
                             ["нет" "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" "11" "12" "13" "14" "14 и более..."]
                             childs-2)]]


                ;; [:div {:class "form-group"}
                ;;  [:label {:class "col-sm-3 control-label" :for "hotel"} "Отель"]
                ;;  [:div {:class "col-sm-9"}
                ;;   [:input {:id "hotel" :name "hotel"
                ;;            :class "form-control"
                ;;            :placeholder "Название отеля, где вы желали бы отдыхать...", :type "text"
                ;;            :value hotel}]]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "wishes"} "Пожелания по отдыху"]
                 [:div {:class "col-sm-9"}
                  [:textarea {:id "wishes" :name "wishes"
                              :class "form-control" :rows "4"
                              :placeholder "название отеля, звездность и другие пожелания по отдыху", :type "text"}
                   wishes]]]


                ;; [:div {:class "form-group"}
                ;;  [:label {:class "col-sm-3 control-label" :for "rating-1"} "Рейтинг отеля:"]
                ;;  [:div {:class "col-sm-9"}
                ;;   [:div {:class "input-group"}

                ;;    [:label {:class "col-sm-3 control-label" :for "rating-1"} "от:"]
                ;;    (drop-down {:class "form-control"} :rating-1
                ;;               ["нет" "1 звезд" "2 звезды" "3 звезды" "4 звезды" "5 звезд"]
                ;;               rating-1)
                ;;    [:label {:class "col-sm-3 control-label" :for "rating-1"} "до:"]
                ;;    (drop-down {:class "form-control"} :rating-2
                ;;               ["нет" "1 звезд" "2 звезды" "3 звезды" "4 звезды" "5 звезд"]
                ;;               rating-2)

                ;;    ]]]


                ;; [:div {:class "form-group"}
                ;;  [:label {:class "col-sm-3 control-label" :for "coastline"} "береговая линия:"]
                ;;  [:div {:class "col-sm-12 col-md-4 col-lg-3"}
                ;;   [:label {:class "radio-inline"}
                ;;    [:input {:type "radio" :name "coastline" :id "coastline1" :value "0"} "нет"]]
                ;;   [:label {:class "radio-inline"}
                ;;    [:input {:type "radio" :name "coastline" :id "coastline2" :value "1"} "Первая"]]
                ;;   [:label {:class "radio-inline"}
                ;;    [:input {:type "radio" :name "coastline" :id "coastline3" :value "2"} "Вторая"]]
                ;;   ]]


                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "name"} "Имя"]
                 [:div {:class "col-sm-9"}
                  [:input {:id "name" :name "name"
                           :class "form-control" :required true
                           :placeholder "Укажите ваше имя...", :type "text"
                           :value name}]]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "email"} "Почта"]
                 [:div {:class "col-sm-9"}
                  [:input {:id "email" :name "email"
                           :class "form-control" :required true
                           :placeholder "Укажите адрес электронной почты...", :type "email"
                           :value email}]]]

                [:div {:class "form-group"}
                 [:label {:class "col-sm-3 control-label" :for "phone"} "Телефон"]
                 [:div {:class "col-sm-9"}
                  [:input {:id "phone" :name "phone"
                           :class "form-control" :required true
                           :placeholder "Укажите номер вашего телефона...", :type "tel"
                           :value phone}]]]





                [:button {:type "submit" :class "btn btn-primary col-sm-offset-10 col-md-offset-10 col-lg-offset-10 col-sm-2 col-md-2 col-lg-2"} "Отправить запрос "
                 [:span {:class "glyphicon glyphicon-send"
                         :style "font-size:1em" :aria-hidden "true"}]]
                [:br]
                [:br]
                ])
      ]]
    )))
