(ns newvoyage.core

  (:use korma.db)
  (:use korma.core)
  (:use clojure.pprint)

  (:require [ixinfestor.core :as ix]
            [ixinfestor.core-handler :as ixch]

            [clj-time.core :as tco]
            [clj-time.format :as tf]
            [clj-time.coerce :as tc]
            [clj-time.local :as tl])
  )




;;**************************************************************************************************
;;* BEGIN Data base initialisation
;;* tag: <database initialisation connection>
;;*
;;* description: инициализация базы данных
;;*
;;**************************************************************************************************
(korma.db/defdb newvoyage (korma.db/postgres {:db "newvoyage"
                                              :user "newvoyage"
                                              :password "paradox"
                                              ;; optional keys
                                              :host "localhost"
                                              :port "5433"}))
;; END Data base initialisation
;;..................................................................................................

(def webdoc
  (-> ix/webdoc
      (prepare (fn [row]
                 (->> row
                      (ix/prepare-date-to-sql-date :showbdate)
                      (ix/prepare-date-to-sql-date :showedate))))
      (transform (fn [row]
                   (->> row
                        (ix/transform-sql-date-to-date :showbdate)
                        (ix/transform-sql-date-to-date :showedate))))
      ))


(defn webdoc-save
  "Сохранение webdoc"
  [webdoc-row]
  (ix/com-save-for-id webdoc webdoc-row))

(def webdoc-select* (select* webdoc))

(defn webdoc-search [query]
  (-> webdoc-select*
      (ix/com-pred-full-text-search* :fts query)
      exec))


;;**************************************************************************************************
;;* BEGIN Roles
;;* tag: <roles>
;;*
;;* description: определяем роли для схемы учета
;;*
;;**************************************************************************************************

(def webrole-admin (ix/webrole-init :admin  "Адмнистратор" "Управление рользователями и настроуками сайта"))
(def webrole-user (ix/webrole-init :user "Пользователь" "Разрешен вход в систему"))
(def webrole-content-manager (ix/webrole-init :content-manager "Редактор контента" "Разрешено редактирование контента"))

(comment
  (let [u (ix/webuser-save {:username "pk" :password (cemerick.friend.credentials/hash-bcrypt "paradox")})]
    (ix/webuserwebrole-add-rels-for-keynames u #{:admin :user :content-manager})
    )
  )



;; END Roles
;;..................................................................................................


(deliver ixinfestor.core/files-root-directory "files_root_directory/vta-data")



(def stext-contacts-key
  (:keyname
   (ix/stext-save-const {:keyname :CONTACTS
                         :description "Поле контактов на главной странице"
                         })))


(def stext-META_COPYRIGHT
  (:keyname
   (ix/stext-save-const {:keyname :META_COPYRIGHT
                         :description "Указать наименование компании на русском и английском языках"
                         })))


(def stext-META_AUTHOR
  (:keyname
   (ix/stext-save-const {:keyname :META_AUTHOR
                         :description "Указать автора"
                         })))


(def stext-META_REGION
  (:keyname
   (ix/stext-save-const {:keyname :META_REGION
                         :description "Указать регион к которому пренадлежит сайт"})))



(def stext-HOME_META_TITLE
  (:keyname
   (ix/stext-save-const {:keyname :HOME_META_TITLE
                         :description "Название в заколовке"})))

(def stext-HOME_META_REGION
  (:keyname
   (ix/stext-save-const {:keyname :HOME_META_REGION
                         :description "Указать регион к которому пренадлежит сайт"})))


(def stext-HOME_META_KEYWORDS
  (:keyname
   (ix/stext-save-const {:keyname :HOME_META_KEYWORDS
                         :description "Общие ключевые слова для сайта"})))


(def stext-HOME_META_DESCRIPTION
  (:keyname
   (ix/stext-save-const {:keyname :HOME_META_DESCRIPTION
                         :description "Общее описание сайта"})))


(def stext-HOME_META_ABSTRACT
  (:keyname
   (ix/stext-save-const {:keyname :HOME_META_ABSTRACT
                         :description "Описание"})))


(def stext-HOME_META_SUBJECT
  (:keyname
   (ix/stext-save-const {:keyname :HOME_META_SUBJECT
                         :description "Тема сайта"})))



(def tag-PHOTO-OF-THE-WEEK
  (:keyname
   (ix/stext-save-const {:keyname :PHOTO-OF-THE-WEEK
                         :description "Фото недели"})))

(def tag-PHOTO-OF-THE-WEEK-COLOR-1
  (:keyname
   (ix/stext-save-const {:keyname :PHOTO-OF-THE-WEEK-COLOR-1
                         :description "Цвет надписи"})))

(def tag-PHOTO-OF-THE-WEEK-COLOR-2
  (:keyname
   (ix/stext-save-const {:keyname :PHOTO-OF-THE-WEEK-COLOR-2
                         :description "Цвет тени"})))








(def tag-countries (ix/tag-save-const {:constname :countries :tagname "Страны" :parent_id nil}))
(def tag-doc-types (ix/tag-save-const {:constname :doctypes :tagname "Типы документов" :parent_id nil}))

(def tag-news
  (ix/tag-save-const {:constname :news
                      :tagname "Новость"
                      :parent_id (:id tag-doc-types)}))

(def tag-advert
  (ix/tag-save-const {:constname :advert
                      :tagname "Объявление"
                      :parent_id (:id tag-doc-types)}))

(def tag-standart-offer
  (ix/tag-save-const {:constname :standart-offer
                      :tagname "Стандартное"
                      :parent_id (:id tag-advert)}))

(def tag-special-offer
  (ix/tag-save-const {:constname :special-offer
                      :tagname "Спецпредложение"
                      :parent_id (:id tag-advert)}))

(def tag-special-offer-fire
  (ix/tag-save-const {:constname :special-offer-fire
                      :tagname "Горящий тур"
                      :parent_id (:id tag-special-offer)}))

(def tag-special-offer-plane
  (ix/tag-save-const {:constname :special-offer-plane
                      :tagname "Вылет"
                      :parent_id (:id tag-special-offer)}))

(def tag-article
  (ix/tag-save-const {:constname :article
                      :tagname "Статья"
                      :parent_id (:id tag-doc-types)}))



(def tag-place
  (ix/tag-save-const {:constname :place
                      :tagname "Описание места"
                      :parent_id (:id tag-doc-types)}))

(def tag-place-type
  (ix/tag-save-const {:constname :place-type
                      :tagname "Тип места"
                      :parent_id (:id tag-place)}))



(def tag-hotel
  (ix/tag-save-const {:constname :hotel
                      :tagname "Отель"
                      :parent_id (:id tag-doc-types)}))


(def tag-hotel-rating
  (ix/tag-save-const {:constname :hotel-rating
                      :tagname "Звездность"
                      :parent_id (:id tag-hotel)}))

(def tag-hotel-rating-1 (ix/tag-save-const {:constname :hotel-rating-1 :tagname "1 звезда" :parent_id (:id tag-hotel-rating)}))
(def tag-hotel-rating-2 (ix/tag-save-const {:constname :hotel-rating-2 :tagname "2 звезды" :parent_id (:id tag-hotel-rating)}))
(def tag-hotel-rating-3 (ix/tag-save-const {:constname :hotel-rating-3 :tagname "3 звезды" :parent_id (:id tag-hotel-rating)}))
(def tag-hotel-rating-4 (ix/tag-save-const {:constname :hotel-rating-4 :tagname "4 звезды" :parent_id (:id tag-hotel-rating)}))
(def tag-hotel-rating-5 (ix/tag-save-const {:constname :hotel-rating-5 :tagname "5 звезд!" :parent_id (:id tag-hotel-rating)}))


(def tag-hotel-rating
  (ix/tag-save-const {:constname :hotel-beachline
                      :tagname "Береговая линия"
                      :parent_id (:id tag-hotel)}))

(def tag-hotel-beachline-1
  (ix/tag-save-const {:constname :hotel-beachline-1 :tagname "1 береговая линия" :parent_id (:id tag-hotel-rating)}))
(def tag-hotel-beachline-2
  (ix/tag-save-const {:constname :hotel-beachline-2 :tagname "2 береговая линия" :parent_id (:id tag-hotel-rating)}))



;;(def tag-com-attrs (ix/tag-save-const {:constname :com-attrs :tagname "Еще..." :parent_id nil}))

(def tag-rest-type
  (ix/tag-save-const {:constname :rest-type
                      :tagname "Типы отдыха"
                      :parent_id nil}))






;; -------------------------------------------------------------------------------------------------

#_(defn migration-start []

    (delete ix/files_rel)
    (delete ix/files)
    (delete ix/webdoctag)
    (delete ix/webdoc)
    (delete ix/stext)
    ;;(delete tag)


    (def tag-countries (ix/tag-save-const {:constname :countries :tagname "Страны" :parent_id nil}))
    (def tag-doc-types (ix/tag-save-const {:constname :doctypes :tagname "Типы документов" :parent_id nil}))

    (def tag-news
      (ix/tag-save-const {:constname :news
                          :tagname "Новость"
                          :parent_id (:id tag-doc-types)}))




    (def tag-advert
      (ix/tag-save-const {:constname :advert
                          :tagname "Объявление"
                          :parent_id (:id tag-doc-types)}))

    (def tag-standart-offer
      (ix/tag-save-const {:constname :standart-offer
                          :tagname "Стандартное"
                          :parent_id (:id tag-advert)}))

    (def tag-special-offer
      (ix/tag-save-const {:constname :special-offer
                          :tagname "Спецпредложение"
                          :parent_id (:id tag-advert)}))




    (def tag-article
      (ix/tag-save-const {:constname :article
                          :tagname "Статья"
                          :parent_id (:id tag-doc-types)}))



    (def tag-place
      (ix/tag-save-const {:constname :place
                          :tagname "Описание места"
                          :parent_id (:id tag-doc-types)}))

    (def tag-place-type
      (ix/tag-save-const {:constname :place-type
                          :tagname "Тип места"
                          :parent_id (:id tag-place)}))



    (def tag-hotel
      (ix/tag-save-const {:constname :hotel
                          :tagname "Отель"
                          :parent_id (:id tag-doc-types)}))


    (def tag-hotel-rating
      (ix/tag-save-const {:constname :hotel-rating
                          :tagname "Звездность"
                          :parent_id (:id tag-hotel)}))

    (def tag-hotel-rating-1 (ix/tag-save-const {:constname :hotel-rating-1 :tagname "1 звезда" :parent_id (:id tag-hotel-rating)}))
    (def tag-hotel-rating-2 (ix/tag-save-const {:constname :hotel-rating-2 :tagname "2 звезды" :parent_id (:id tag-hotel-rating)}))
    (def tag-hotel-rating-3 (ix/tag-save-const {:constname :hotel-rating-3 :tagname "3 звезды" :parent_id (:id tag-hotel-rating)}))
    (def tag-hotel-rating-4 (ix/tag-save-const {:constname :hotel-rating-4 :tagname "4 звезды" :parent_id (:id tag-hotel-rating)}))
    (def tag-hotel-rating-5 (ix/tag-save-const {:constname :hotel-rating-5 :tagname "5 звезд!" :parent_id (:id tag-hotel-rating)}))


    (def tag-hotel-rating
      (ix/tag-save-const {:constname :hotel-beachline
                          :tagname "Береговая линия"
                          :parent_id (:id tag-hotel)}))

    (def tag-hotel-beachline-1
      (ix/tag-save-const {:constname :hotel-beachline-1 :tagname "1 береговая линия" :parent_id (:id tag-hotel-rating)}))
    (def tag-hotel-beachline-2
      (ix/tag-save-const {:constname :hotel-beachline-2 :tagname "2 береговая линия" :parent_id (:id tag-hotel-rating)}))



    ;;(def tag-com-attrs (ix/tag-save-const {:constname :com-attrs :tagname "Еще..." :parent_id nil}))

    (def tag-rest-type
      (ix/tag-save-const {:constname :rest-type
                          :tagname "Типы отдыха"
                          :parent_id nil}))


    ;; DATA MIGRATION

    (defonce voyager (create-db (korma.db/postgres {:db "voyager"
                                                    :user "voyager"
                                                    :password "paradox#65535"
                                                    ;; optional keys
                                                    :host "localhost"
                                                    :port "5433"})))


    ;;
    (defn tag-delete-all-childs-for-id [id]
      (let [childs (select ix/tag (where (= :parent_id id)))]
        (println id (count childs))

        (when-not (empty? childs)
          (doall (map (comp tag-delete-all-childs-for-id :id) childs)))

        (println id)

        (delete ix/tag (where (= :parent_id id)))))
    ;;


    (println "\n*** Импорт типов отдыха ***")

    (defentity resorttype
      (pk :id)
      (database voyager))

    (def resorttype-id-id
      (let [p-id (:id tag-rest-type)]

        (tag-delete-all-childs-for-id p-id)

        (->> (select resorttype)
             (reduce
              (fn [a {:keys [id keyname description]}]
                (println keyname description)
                (assoc a id (:id (ix/tag-save {:tagname keyname
                                               :description description
                                               :parent_id p-id}))))
              {}))))


    (println "\n*** Импорт типов мест ***")

    (defentity ltype
      (pk :id)
      (database voyager))


    (def ltype-id-id
      (let [p-id (:id tag-place-type)]

        (tag-delete-all-childs-for-id p-id)

        (->> (select ltype)
             (reduce
              (fn [a {:keys [id keyname description]}]
                (println keyname description)
                (assoc a id (:id (ix/tag-save {:tagname keyname
                                               :description description
                                               :parent_id p-id}))))
              {}))))


    (println "\n*** Импорт мест ***")



    (defentity location_images
      (database voyager))


    (defentity tlocation
      (pk :id)
      (has-many location_images {:fk :location_id})
      (database voyager))




    (defentity hotel_images
      (database voyager))

    (defentity hotel_resort_types
      (database voyager))

    (defentity hotel
      (pk :id)
      (has-many hotel_images {:fk :hotel_id})
      (has-many hotel_resort_types {:fk :hotel_id})
      (database voyager))


    (defentity news_images
      (database voyager))

    (defentity news
      (pk :id)
      (has-many news_images {:fk :news_id})
      (database voyager))

    (defentity announce_images
      (database voyager))

    (defentity announce
      (pk :id)
      (has-many announce_images {:fk :announce_id})
      (database voyager))


    (defentity publicimage
      (pk :id)
      (database voyager))

    (defentity file_resourse
      (pk :id)
      (database voyager))


    (defentity anytext
      (pk :id)
      (database voyager))



    ;; ---------

    (defentity webdoc-direct
      (table :webdoc)
      (pk :id))

    ;; ---------





    (def tlocation-id-id

      (let [p-id (:id tag-countries)
            _ (tag-delete-all-childs-for-id p-id)
            m (->> (select tlocation)
                   (reduce
                    (fn [a {:keys [id title parent_id]}]
                      (println id title)
                      (assoc a id (-> {:tagname title :parent_id p-id}
                                      ix/tag-save
                                      (assoc :parent_id parent_id))))
                    {}))
            ;;_ (println m)
            _ (->> m vals
                   (map #(-> %
                             (update-in [:parent_id] (fn [x] (or (-> x m :id) p-id)))
                             ix/tag-save))
                   doall)

            m (->> m seq (reduce #(assoc % (first %2) (or (-> %2 second :id) p-id)) {}))

            ]

        (doseq [{:keys [id description
                        ltype_id title parent_id
                        meta_description meta_keywords meta_subject location_images]}
                (select tlocation (with location_images))]
          (let [webdoc-row (ix/webdoc-save {:keyname title
                                            :web_description (-> description
                                                                 (clojure.string/replace #"/vta/mvc/" "/")
                                                                 (clojure.string/replace #"/vta/Image?file=" "/image/"))
                                            :web_meta_subject meta_subject
                                            :web_meta_keywords  meta_keywords
                                            :web_meta_description meta_description

                                            })]
            (ix/webdoctag-add-tag webdoc-row tag-place)
            (ix/webdoctag-add-tag webdoc-row {:id (ltype-id-id ltype_id)})
            (ix/webdoctag-add-tag webdoc-row {:id (-> (select ix/tag (where (= :tagname title))) first :id (or p-id))})

            (doseq [{:keys [filename]} location_images]
              (let [{file-id :id} (ix/files-save {:path filename
                                                  :filename (-> filename (clojure.string/split #"/") last)
                                                  :content_type (ring.util.mime-type/ext-mime-type filename)
                                                  })]
                (insert ix/files_rel (values {:files_id file-id :webdoc_id (webdoc-row :id)}))
                ))
            ))


        ;; Hotels
        (doseq [{:keys [id keyname description
                        rating location_id beachline
                        meta_description meta_keywords meta_subject
                        hotel_resort_types hotel_images]}
                (select hotel (with hotel_resort_types) (with hotel_images))]
          (transaction
           (let [webdoc-row (ix/webdoc-save {:keyname keyname
                                             :web_description (-> description
                                                                  (clojure.string/replace #"/vta/mvc/" "/")
                                                                  (clojure.string/replace #"/vta/Image?file=" "/image/"))
                                             :web_meta_subject meta_subject
                                             :web_meta_keywords  meta_keywords
                                             :web_meta_description meta_description

                                             })]

             (ix/webdoctag-add-tag webdoc-row tag-hotel)
             (ix/webdoctag-add-tag webdoc-row {:id (or (m location_id) p-id)})

             (condp = beachline
               1 (ix/webdoctag-add-tag webdoc-row tag-hotel-beachline-1)
               2 (ix/webdoctag-add-tag webdoc-row tag-hotel-beachline-2)
               (println 0))

             (when-let [r ({1 tag-hotel-rating-1
                            2 tag-hotel-rating-2
                            3 tag-hotel-rating-3
                            4 tag-hotel-rating-4
                            5 tag-hotel-rating-5} rating)]
               (ix/webdoctag-add-tag webdoc-row r))


             (doseq [{id :resort_type_id} hotel_resort_types]
               (ix/webdoctag-add-tag webdoc-row {:id (resorttype-id-id id)}))


             (doseq [{:keys [filename]} hotel_images]
               (let [{file-id :id} (or (-> (select ix/files (where (= :path filename))) first)
                                       (ix/files-save {:path filename
                                                       :filename (-> filename (clojure.string/split #"/") last)
                                                       :content_type (ring.util.mime-type/ext-mime-type filename)
                                                       }))]
                 (println ">>>" filename)
                 (when (-> (select ix/files_rel
                                   (where (and (= :files_id file-id) (= :webdoc_id (webdoc-row :id)))))
                           empty?)
                   (insert ix/files_rel (values {:files_id file-id :webdoc_id (webdoc-row :id)}))
                   )
                 ))






             )))

        (doseq [{:keys [id keyname description
                        cdate
                        titleimageurl topdescription
                        publication
                        meta_description meta_keywords meta_subject
                        news_resort_types news_images]}
                (select news (with news_images))]
          (transaction
           (let [webdoc-row (ix/webdoc-save {:keyname keyname
                                             :cdate cdate
                                             :web_title_image (-> titleimageurl
                                                                  (clojure.string/replace #"/mvc/image/" "")
                                                                  (clojure.string/replace #"/vta/Image?file=" ""))
                                             :web_top_description (-> topdescription ix/html-clean-tags)
                                             :web_description (-> description
                                                                  (clojure.string/replace #"/vta/mvc/" "/")
                                                                  (clojure.string/replace #"/vta/Image?file=" "/image/"))
                                             :web_meta_subject meta_subject
                                             :web_meta_keywords  meta_keywords
                                             :web_meta_description meta_description
                                             })]

             (update webdoc-direct
                     (set-fields {:cdate cdate})
                     (where (= :id (:id webdoc-row))))

             (ix/webdoctag-add-tag webdoc-row tag-news)

             (when publication
               (ix/webdoctag-add-tag webdoc-row tag-article))

             (doseq [{:keys [filename]} news_images]
               (let [{file-id :id} (or (-> (select ix/files (where (= :path filename))) first)
                                       (ix/files-save {:path filename
                                                       :filename (-> filename (clojure.string/split #"/") last)
                                                       :content_type (ring.util.mime-type/ext-mime-type filename)
                                                       }))]
                 (println ">>>" filename)
                 (when (-> (select ix/files_rel
                                   (where (and (= :files_id file-id) (= :webdoc_id (webdoc-row :id)))))
                           empty?)
                   (insert ix/files_rel (values {:files_id file-id :webdoc_id (webdoc-row :id)}))
                   )
                 ))

             )))


        ;; подумать над реализацией атрибутов
        (doseq [{:keys [id keyname description
                        cdate
                        titleimageurl topdescription
                        publication
                        meta_description meta_keywords meta_subject
                        show
                        showbdate
                        showedate
                        prise
                        rukushka
                        announce_resort_types announce_images]}
                (select announce (with announce_images))]
          (transaction
           (let [webdoc-row (ix/webdoc-save {:keyname keyname
                                             :cdate cdate
                                             :web_title_image (-> titleimageurl
                                                                  (clojure.string/replace #"/mvc/image/" "")
                                                                  (clojure.string/replace #"/vta/Image?file=" ""))
                                             :web_top_description (-> topdescription ix/html-clean-tags)
                                             :web_description (-> description
                                                                  (clojure.string/replace #"/vta/mvc/" "/")
                                                                  (clojure.string/replace #"/vta/Image?file=" "/image/"))
                                             :web_meta_subject meta_subject
                                             :web_meta_keywords  meta_keywords
                                             :web_meta_description meta_description
                                             :showing show
                                             :showbdate showbdate
                                             :showedate showedate
                                             :price prise
                                             })]

             (println "rukushka " rukushka)

             (ix/webdoctag-add-tag webdoc-row tag-advert)

             (if rukushka
               (ix/webdoctag-add-tag webdoc-row tag-standart-offer)
               (ix/webdoctag-add-tag webdoc-row tag-special-offer))



             (doseq [{:keys [filename]} announce_images]
               (let [{file-id :id} (or (-> (select ix/files (where (= :path filename))) first)
                                       (ix/files-save {:path filename
                                                       :filename (-> filename (clojure.string/split #"/") last)
                                                       :content_type (ring.util.mime-type/ext-mime-type filename)
                                                       }))]
                 (println ">>>" filename)
                 (when (-> (select ix/files_rel
                                   (where (and (= :files_id file-id) (= :webdoc_id (webdoc-row :id)))))
                           empty?)
                   (insert ix/files_rel (values {:files_id file-id :webdoc_id (webdoc-row :id)})) )))

             )))



        (doseq [{:keys [filename]} (select file_resourse)]
          (let [{file-id :id} (or (-> (select ix/files (where (= :path filename))) first)
                                  (when filename
                                    (ix/files-save {:path filename
                                                    :filename (-> filename (clojure.string/split #"/") last)
                                                    :content_type (ring.util.mime-type/ext-mime-type filename)
                                                    })))]
            (println "1>>>" filename)))


        (doseq [{:keys [filename] :as row} (select publicimage)
                :when (not (nil? filename))]
          (let [{file-id :id} (or (-> (select ix/files (where (= :path filename))) first)
                                  (ix/files-save {:path filename
                                                  :filename (-> filename (clojure.string/split #"/") last)
                                                  :content_type (ring.util.mime-type/ext-mime-type filename)
                                                  }))]
            (println "2>>>" filename)))

        (println "333")
        (doseq [row (select anytext)]
          (-> row
              (update-in [:anytext] #(if (empty? %) %
                                         (-> %
                                             (clojure.string/replace #"/vta/mvc/" "/")
                                             (clojure.string/replace #"/vta/Image?file=" "/image/"))))
              (dissoc :id)
              ix/stext-save ))

        m))

    ;; Теперь когда вся иерархия построена надо все проиндексировать
    (update ix/tag (set-fields {:fts nil}))

    ) ;; do
