(ns ixinfestor.page-main
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require [hipo.core :as hipo]
            [ixinfestor.core :as ix]

            [ixinfestor.webusers :as webusers]
            [ixinfestor.stext :as stext]
            [ixinfestor.files :as files]
            [ixinfestor.webdocs :as webdocs]
            [ixinfestor.webdocs-edit :as webdocs-edit]
            [ixinfestor.change-password :as change-password]

            [ajax.core :refer [GET POST]]
            [dommy.core :as dommy :refer-macros [sel sel1]]

            [cljs.core.async :as async :refer [>! <! put! chan alts!]]

            [goog.events :as events]
            [goog.dom.classes :as classes]))

(enable-console-print!)


(def page-init-maps (atom {}))

(defn new-chan-repaint [k]
  (let [c (chan)]
    (swap! page-init-maps assoc k c)
    c))

(defn main-set-padding-top-by-navbar []
  (dommy/set-px! (ix/by-id :main) :padding-top (- (dommy/px (ix/by-id :main-navbar) :height) 48)))

(def chan-do-after-repaint (chan))
(go
  (while true
    (let [_ (<! chan-do-after-repaint)]
      (main-set-padding-top-by-navbar)
      )))

(def chan-show-page (chan))
(go
  (while true
    (let [page-key (<! chan-show-page)]
      (println "SWITCH TO PAGE:" page-key)

      (dommy/clear! (ix/by-id :toolbars-container))
      (dommy/clear! (ix/by-id :main))
      (dommy/clear! (ix/by-id :page-caption))

      (when-let [chan-repaint (@page-init-maps page-key)]
        (put! chan-repaint chan-do-after-repaint)))))

(defn ^:export init []
  (when (and js/document (.-getElementById js/document))
    (println "INIT MAIN PAGE [START]")

    (dommy/append!
     (ix/by-id :navbar)
     (hipo/create
      (ix/navbar (list
                  (ix/nav-menu-item "Сайт" "glyphicon-th-large" {:target "_blank" :href "/"})

                  (ix/nav-menu "Справочники" "glyphicon-book" {}
                               (list
                                (ix/nav-menu-item "Документы" "glyphicon-book"
                                                  {:on-click #(put! chan-show-page :webdocs)})
                                (ix/nav-menu-item "Текстовые поля" "glyphicon-book"
                                                  {:on-click #(put! chan-show-page :stext)})
                                (ix/nav-menu-item "Файлы и картинки" "glyphicon-book"
                                                  {:on-click #(put! chan-show-page :files)})
                                ))

                  (ix/nav-menu "Администрирование" "glyphicon-cog" {}
                               (list
                                (ix/nav-menu-item "Пользователи" "glyphicon-user"
                                                  {:on-click #(put! chan-show-page :webusers)})
                                ))

                  (ix/nav-menu (dommy/value (ix/by-id "username"))
                               "glyphicon-user" {}
                               (list
                                #_(ix/nav-menu-item "Профиль" "glyphicon-barcode"
                                                    {:on-click #(put! chan-show-page :users)})
                                (ix/nav-menu-item "Сменить пароль..." "glyphicon-lock"
                                                  {:on-click #(put! chan-show-page :change-password)})
                                [:li.divider]
                                (ix/nav-menu-item "Выход" "glyphicon-log-out"
                                                  {:on-click #(-> js/window .-location .-href (set! "/logout"))})
                                ))


                  ))))

    ;; (.addEventListener js/document "DOMSubtreeModified"
    ;;                    #(do
    ;;                       (main-set-padding-top-by-navbar)
    ;;                       (println "<--H-->")))

    ;;(put! chan-show-page 1)

    (println "INIT MAIN PAGE [END]")))

(def webusers-map
  (let [m (webusers/webusers-new
           chan-show-page
           chan-do-after-repaint
           {})]

    (swap! page-init-maps assoc :webusers (m :chan-repaint))
    (swap! page-init-maps assoc :webusers-edit (-> m :webusers-edit-rmap :chan-repaint))

    m))


(def change-password-map
  (let [m (change-password/change-password-new
           chan-show-page
           chan-do-after-repaint
           {})]
    (swap! page-init-maps assoc :change-password (m :chan-repaint))
    m))



(def stext-map
  (let [m (stext/stext-new
           chan-show-page
           chan-do-after-repaint
           {})]

    (swap! page-init-maps assoc :stext (m :chan-repaint))
    (swap! page-init-maps assoc :stext-edit (-> m :stext-edit-rmap :chan-repaint))
    m))


(def files-map
  (let [m (files/files-new
           chan-show-page
           chan-do-after-repaint
           {})]

    (swap! page-init-maps assoc :files (m :chan-repaint))
    m))




(def webdocs-map
  (let [m (webdocs/webdocs-new
           chan-show-page
           chan-do-after-repaint
           {webdocs/webdocs-edit-imap-key
            {webdocs-edit/specific-common-key
             {webdocs-edit/specific-common-inputs-fn-key
              (fn [row]
                (list

                 [:fieldset
                  [:legend "Данные для объявлений"]
                  [:div {:class "form-group"}
                   [:label {:class "col-sm-2 control-label" :for "input-showing"} "Видимость"]
                   [:div {:class "col-sm-10"}
                    [:div {:class "checkbox"}
                     [:label
                      [:input {:id "input-showing" :type "checkbox"
                               :checked (get-in row [:webdoc-row :showing] true)}]
                      "показывать"]]]]

                  [:div {:class "form-group"}
                   [:label {:class "col-sm-2 control-label" :for "input-orderindex"} "порядок"]
                   [:div {:class "col-sm-3"}
                    [:input {:id "input-orderindex" :class "form-control"
                             :type "number" :min "0" :step "1"
                             :value (or (get-in row [:webdoc-row :orderindex]) 100) }]]
                   ]


                  [:div {:class "form-group"}
                   [:label {:class "col-sm-2 control-label" :for "input-showbdate"} "Публиковать"]

                   [:label {:class "col-sm-1 control-label" :for "input-showbdate"} "с"]
                   [:div {:class "col-sm-3"}
                    [:input {:id "input-showbdate" :class "form-control"
                             :type "date"
                             :pattern "\\d{4}-\\d{2}-\\d{2}"
                             :placeholder "yyyy-MM-dd"
                             :value
                             (ix/format-date "yyyy-MM-dd"
                                             (if-let [ds (get-in row [:webdoc-row :showbdate])]
                                               (ix/str-to-date ds)
                                               (new js/Date)))
                             }]]
                   [:label {:class "col-sm-1 control-label" :for "input-showedate"} "до"]
                   [:div {:class "col-sm-3"}
                    [:input {:id "input-showedate" :class "form-control"
                             :type "date"
                             :pattern "\\d{4}-\\d{2}-\\d{2}"
                             :placeholder "yyyy-MM-dd"
                             :value
                             (ix/format-date "yyyy-MM-dd"
                                             (if-let [ds (get-in row [:webdoc-row :showedate])]
                                               (ix/str-to-date ds)
                                               (new js/Date)))
                             }]]]

                  [:div {:class "form-group"}
                   [:label {:class "col-sm-2 control-label" :for "input-price"} "Цена"]
                   [:div {:class "col-sm-3"}
                    [:input {:id "input-price" :class "form-control"
                             :type "number" :min "0.01" :step "1"
                             :value (or (get-in row [:webdoc-row :price]) 0) }]]
                   ]

                  [:div {:class "form-group"}
                   [:label {:class "col-sm-2 control-label" :for "select-currency"} "Валюта"]
                   [:div {:class "col-sm-4"}
                    [:select {:id "select-currency" :class "btn btn-default dropdown-toggle navbar-left select-ca-root"}
                     (let [v (get-in row [:webdoc-row :currency])]
                       (list
                        [:option {:value "0 KZT" :selected (= v "0 KZT")} "KZT"]
                        [:option {:value "1 USD" :selected (= v "1 USD")} "USD"]
                        [:option {:value "2 EUR" :selected (= v "2 EUR")} "EUR"]
                        )
                       )
                     ]
                    ]
                   ]


                  ]

                 ))

              webdocs-edit/specific-common-save-fn-key
              (fn [row]
                (-> row
                    (assoc :price (-> :input-price ix/by-id dommy/value))
                    (assoc :currency (-> :select-currency ix/by-id dommy/value))
                    (assoc :orderindex (-> :input-orderindex ix/by-id dommy/value))
                    (assoc :showing (-> :input-showing ix/by-id .-checked))
                    (assoc :showbdate (-> :input-showbdate ix/by-id dommy/value ix/str-to-date .getTime))
                    (assoc :showedate (-> :input-showedate ix/by-id dommy/value ix/str-to-date .getTime))
                    ))
              }
             }})]

    (swap! page-init-maps assoc :webdocs (m :chan-repaint))
    (swap! page-init-maps assoc :webdocs-edit (-> m :webdocs-edit-rmap :chan-repaint))

    m))








