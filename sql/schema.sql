--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.4
-- Dumped by pg_dump version 9.4.4
-- Started on 2015-07-03 18:07:03 ALMT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 188 (class 3079 OID 11861)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2106 (class 0 OID 0)
-- Dependencies: 188
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 201 (class 1255 OID 19092)
-- Name: files_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION files_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
BEGIN
	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	
	NEW.fts=(setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.filename),''''),''D'') || '' '' ||		 
		 setweight( coalesce( to_tsvector(''english'',NEW.top_description),''''),''D'') || '' '' ||		 		 	 		 
		 setweight( coalesce( to_tsvector(''english'',NEW.description),''''),''D''));
	
  END IF;
  
  RETURN NEW;
END;';


--
-- TOC entry 202 (class 1255 OID 19093)
-- Name: stext_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION stext_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
BEGIN
	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	
	NEW.fts=(setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.keyname),''''),''D'') || '' '' ||		 
		 setweight( coalesce( to_tsvector(''english'',NEW.description),''''),''D'') || '' '' ||		 		 
		 setweight( coalesce( to_tsvector(''english'',NEW.anytext),''''),''D''));
	
  END IF;
  
  RETURN NEW;
END;';


--
-- TOC entry 203 (class 1255 OID 19094)
-- Name: tag_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION tag_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
	tagpath text;
BEGIN


	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	


	WITH RECURSIVE temp1 ( id, parent_id, tagname ) AS (
	SELECT id, parent_id, tagname, cast (tagname as text) as PATH FROM tag WHERE parent_id IS NULL
	union
	select a.id, a.parent_id, a.tagname, cast (b.PATH ||'' ''|| a.tagname as text) as PATH FROM tag a, temp1 b WHERE a.parent_id = b.id)
	select PATH into tagpath from temp1 WHERE id = NEW.id;


	--RAISE NOTICE ''tagpath (%)'', tagpath;

  
	NEW.fts=(setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.tagname),''''),''D'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.description),''''),''D'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',tagpath),''''),''B'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.constname),''''),''D'')
		 );

	--RAISE NOTICE ''>>> (%)'', NEW.fts;
	
  END IF;
  
  RETURN NEW;
END;';


--
-- TOC entry 204 (class 1255 OID 19095)
-- Name: webdoc_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION webdoc_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
	tag_row RECORD;
	tags_str TEXT := '''';
BEGIN
	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	

	FOR tag_row IN SELECT tagname FROM tag WHERE id IN (SELECT tag_id FROM webdoctag WHERE webdoc_id = NEW.ID)
	LOOP
		tags_str := tags_str || '' '' || tag_row.tagname;
	END LOOP;

  
	NEW.fts=(setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' ||
	
		 setweight( coalesce( to_tsvector(''english'',tags_str),''''),''B'') || '' '' ||
		 
	         setweight( coalesce( to_tsvector(''english'',NEW.keyname),''''),''C'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.web_title),''''),''D'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.web_meta_subject),''''),''D'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.web_meta_keywords),''''),''D'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.web_meta_description),''''),''D'') || '' '' ||
		 
		 setweight( coalesce( to_tsvector(''english'',NEW.web_top_description),''''),''D'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.plan_text),''''),''D''));
	
  END IF;
  
  RETURN NEW;
END;';


--
-- TOC entry 205 (class 1255 OID 19096)
-- Name: webuser_fts_indexation(); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION webuser_fts_indexation() RETURNS trigger
    LANGUAGE plpgsql
    AS 'DECLARE
BEGIN
	
  IF TG_OP = ''INSERT'' OR TG_OP =''UPDATE'' THEN	
	NEW.fts=(setweight( coalesce( to_tsvector(''id '' || NEW.id),''''),''A'') || '' '' ||
		 setweight( coalesce( to_tsvector(''english'',NEW.username),''''),''D'') || '' '' ||		 
		 setweight( coalesce( to_tsvector(''english'',NEW.description),''''),''D''));
	
  END IF;
  
  RETURN NEW;
END;';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 19097)
-- Name: stext; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE stext (
    id bigint NOT NULL,
    keyname character varying(50) NOT NULL,
    description text,
    plantext boolean DEFAULT false NOT NULL,
    anytext text,
    fts tsvector
);


--
-- TOC entry 173 (class 1259 OID 19104)
-- Name: anytext_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE anytext_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2107 (class 0 OID 0)
-- Dependencies: 173
-- Name: anytext_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE anytext_id_seq OWNED BY stext.id;


--
-- TOC entry 174 (class 1259 OID 19106)
-- Name: files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE files (
    id integer NOT NULL,
    path character varying(512) NOT NULL,
    filename character varying(255) NOT NULL,
    urlpath text,
    top_description text,
    description text,
    content_type character varying(255),
    size bigint,
    galleria boolean,
    fts tsvector
);


--
-- TOC entry 175 (class 1259 OID 19112)
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2108 (class 0 OID 0)
-- Dependencies: 175
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE files_id_seq OWNED BY files.id;


--
-- TOC entry 176 (class 1259 OID 19114)
-- Name: files_rel; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE files_rel (
    files_id integer,
    webdoc_id integer
);


--
-- TOC entry 177 (class 1259 OID 19117)
-- Name: tag; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE tag (
    id integer NOT NULL,
    tagname character varying(100),
    description text,
    parent_id integer,
    const boolean,
    constname character varying(50),
    fts tsvector
);


--
-- TOC entry 178 (class 1259 OID 19123)
-- Name: tag_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2109 (class 0 OID 0)
-- Dependencies: 178
-- Name: tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE tag_id_seq OWNED BY tag.id;


--
-- TOC entry 179 (class 1259 OID 19125)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 180 (class 1259 OID 19127)
-- Name: webdoc; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webdoc (
    id integer NOT NULL,
    keyname character varying(255) NOT NULL,
    description text,
    cdate timestamp with time zone,
    udate timestamp with time zone,
    web_title text,
    web_meta_subject text,
    web_meta_keywords text,
    web_meta_description text,
    web_top_description text,
    web_description text,
    fts tsvector,
    web_citems text,
    url1 text,
    url1flag boolean,
    ttitle text,
    web_title_image text,
    plan_text text,
    showbdate date,
    showedate date,
    showing boolean,
    price numeric(20,2),
    orderindex integer,
    currency character varying(10),
    web_color_0 character varying(7),
    web_color_1 character varying(7),
    web_color_2 character varying(7),
    web_color_3 character varying(7),
    web_color_4 character varying(7),
    web_color_5 character varying(7)
);


--
-- TOC entry 181 (class 1259 OID 19133)
-- Name: webdoc_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE webdoc_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2110 (class 0 OID 0)
-- Dependencies: 181
-- Name: webdoc_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE webdoc_id_seq OWNED BY webdoc.id;


--
-- TOC entry 182 (class 1259 OID 19135)
-- Name: webdoctag; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webdoctag (
    webdoc_id integer NOT NULL,
    tag_id integer NOT NULL
);


--
-- TOC entry 183 (class 1259 OID 19138)
-- Name: webrole; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webrole (
    id integer NOT NULL,
    keyname character varying(50) NOT NULL,
    title character varying(255),
    description text
);


--
-- TOC entry 184 (class 1259 OID 19144)
-- Name: webrole_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE webrole_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2111 (class 0 OID 0)
-- Dependencies: 184
-- Name: webrole_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE webrole_id_seq OWNED BY webrole.id;


--
-- TOC entry 185 (class 1259 OID 19146)
-- Name: webuser; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webuser (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    description text,
    password character varying(60),
    fts tsvector
);


--
-- TOC entry 186 (class 1259 OID 19152)
-- Name: webuser_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE webuser_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2112 (class 0 OID 0)
-- Dependencies: 186
-- Name: webuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE webuser_id_seq OWNED BY webuser.id;


--
-- TOC entry 187 (class 1259 OID 19154)
-- Name: webuserwebrole; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE webuserwebrole (
    webuser_id integer NOT NULL,
    webrole_id integer NOT NULL
);


--
-- TOC entry 1942 (class 2604 OID 19157)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY files ALTER COLUMN id SET DEFAULT nextval('files_id_seq'::regclass);


--
-- TOC entry 1941 (class 2604 OID 19158)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY stext ALTER COLUMN id SET DEFAULT nextval('anytext_id_seq'::regclass);


--
-- TOC entry 1943 (class 2604 OID 19159)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag ALTER COLUMN id SET DEFAULT nextval('tag_id_seq'::regclass);


--
-- TOC entry 1944 (class 2604 OID 19160)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoc ALTER COLUMN id SET DEFAULT nextval('webdoc_id_seq'::regclass);


--
-- TOC entry 1945 (class 2604 OID 19161)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY webrole ALTER COLUMN id SET DEFAULT nextval('webrole_id_seq'::regclass);


--
-- TOC entry 1946 (class 2604 OID 19162)
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY webuser ALTER COLUMN id SET DEFAULT nextval('webuser_id_seq'::regclass);


--
-- TOC entry 1948 (class 2606 OID 19164)
-- Name: anytext_keyname_key; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stext
    ADD CONSTRAINT anytext_keyname_key UNIQUE (keyname);


--
-- TOC entry 1950 (class 2606 OID 19166)
-- Name: anytext_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY stext
    ADD CONSTRAINT anytext_pkey PRIMARY KEY (id);


--
-- TOC entry 1952 (class 2606 OID 19168)
-- Name: files_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_pk PRIMARY KEY (id);


--
-- TOC entry 1956 (class 2606 OID 19170)
-- Name: files_rel_webdoc_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files_rel
    ADD CONSTRAINT files_rel_webdoc_uk UNIQUE (files_id, webdoc_id);


--
-- TOC entry 1954 (class 2606 OID 19172)
-- Name: files_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY files
    ADD CONSTRAINT files_uk1 UNIQUE (path);


--
-- TOC entry 1958 (class 2606 OID 19174)
-- Name: tag_const_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_const_uk UNIQUE (const, constname);


--
-- TOC entry 1960 (class 2606 OID 19176)
-- Name: tag_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_pk PRIMARY KEY (id);


--
-- TOC entry 1962 (class 2606 OID 19178)
-- Name: tag_uk1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_uk1 UNIQUE (tagname);


--
-- TOC entry 1964 (class 2606 OID 19180)
-- Name: webdoc_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoc
    ADD CONSTRAINT webdoc_pk PRIMARY KEY (id);


--
-- TOC entry 1966 (class 2606 OID 19182)
-- Name: webdoctag_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webdoctag
    ADD CONSTRAINT webdoctag_pk PRIMARY KEY (webdoc_id, tag_id);


--
-- TOC entry 1968 (class 2606 OID 19184)
-- Name: webrole_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webrole
    ADD CONSTRAINT webrole_pk PRIMARY KEY (id);


--
-- TOC entry 1970 (class 2606 OID 19186)
-- Name: webrole_uk_1; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webrole
    ADD CONSTRAINT webrole_uk_1 UNIQUE (keyname);


--
-- TOC entry 1972 (class 2606 OID 19188)
-- Name: webrole_uk_2; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webrole
    ADD CONSTRAINT webrole_uk_2 UNIQUE (title);


--
-- TOC entry 1974 (class 2606 OID 19190)
-- Name: webuser_pk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webuser
    ADD CONSTRAINT webuser_pk PRIMARY KEY (id);


--
-- TOC entry 1976 (class 2606 OID 19192)
-- Name: webuser_username_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webuser
    ADD CONSTRAINT webuser_username_uk UNIQUE (username);


--
-- TOC entry 1978 (class 2606 OID 19194)
-- Name: webuserwebrole_uk; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY webuserwebrole
    ADD CONSTRAINT webuserwebrole_uk PRIMARY KEY (webuser_id, webrole_id);


--
-- TOC entry 1987 (class 2620 OID 19195)
-- Name: files_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER files_fts_indexation_tg BEFORE INSERT OR UPDATE ON files FOR EACH ROW EXECUTE PROCEDURE files_fts_indexation();


--
-- TOC entry 1986 (class 2620 OID 19196)
-- Name: stext_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER stext_fts_indexation_tg BEFORE INSERT OR UPDATE ON stext FOR EACH ROW EXECUTE PROCEDURE stext_fts_indexation();


--
-- TOC entry 1988 (class 2620 OID 19197)
-- Name: tag_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER tag_fts_indexation_tg BEFORE INSERT OR UPDATE ON tag FOR EACH ROW EXECUTE PROCEDURE tag_fts_indexation();


--
-- TOC entry 1989 (class 2620 OID 19198)
-- Name: webdoc_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER webdoc_fts_indexation_tg BEFORE INSERT OR UPDATE ON webdoc FOR EACH ROW EXECUTE PROCEDURE webdoc_fts_indexation();


--
-- TOC entry 1990 (class 2620 OID 19199)
-- Name: webuser_fts_indexation_tg; Type: TRIGGER; Schema: public; Owner: -
--

CREATE TRIGGER webuser_fts_indexation_tg BEFORE INSERT OR UPDATE ON webuser FOR EACH ROW EXECUTE PROCEDURE webuser_fts_indexation();


--
-- TOC entry 1979 (class 2606 OID 19200)
-- Name: files_rel_to_files_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY files_rel
    ADD CONSTRAINT files_rel_to_files_id_fk FOREIGN KEY (files_id) REFERENCES files(id);


--
-- TOC entry 1980 (class 2606 OID 19205)
-- Name: files_rel_webdoc_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY files_rel
    ADD CONSTRAINT files_rel_webdoc_fk FOREIGN KEY (webdoc_id) REFERENCES webdoc(id);


--
-- TOC entry 1981 (class 2606 OID 19210)
-- Name: tag_tree_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY tag
    ADD CONSTRAINT tag_tree_fk FOREIGN KEY (parent_id) REFERENCES tag(id);


--
-- TOC entry 1982 (class 2606 OID 19215)
-- Name: webdoctag_tag_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoctag
    ADD CONSTRAINT webdoctag_tag_id FOREIGN KEY (tag_id) REFERENCES tag(id);


--
-- TOC entry 1983 (class 2606 OID 19220)
-- Name: webdoctag_webdoc_id; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webdoctag
    ADD CONSTRAINT webdoctag_webdoc_id FOREIGN KEY (webdoc_id) REFERENCES webdoc(id);


--
-- TOC entry 1984 (class 2606 OID 19225)
-- Name: webuserwebrole_webrole_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webuserwebrole
    ADD CONSTRAINT webuserwebrole_webrole_id_fk FOREIGN KEY (webrole_id) REFERENCES webrole(id);


--
-- TOC entry 1985 (class 2606 OID 19230)
-- Name: webuserwebrole_webuser_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY webuserwebrole
    ADD CONSTRAINT webuserwebrole_webuser_id_fk FOREIGN KEY (webuser_id) REFERENCES webuser(id);


-- Completed on 2015-07-03 18:07:03 ALMT

--
-- PostgreSQL database dump complete
--

