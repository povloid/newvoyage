#/sbin/sh

##export PATH=$PATH:/opt/PostgreSQL/9.3/bin/
export PGPASSWORD="paradox"

dropdb -e -h localhost -U newvoyage -p 5433 newvoyage
createdb -h localhost -U newvoyage -p 5433 -e -E UTF8 -T template0 newvoyage
psql -h localhost -U newvoyage -p 5433 -f schema.sql newvoyage

