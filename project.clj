(defproject newvoyage "0.1.0-SNAPSHOT"
  :description "Сайт vta.kz"
  :url "http://vta.kz"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [tarsonis "0.1.0"]

                 [org.clojure/tools.nrepl "0.2.10"]
                 [cider/cider-nrepl "0.9.1"]

                 [clj-http "1.1.2"] ;; Клиент для импорта данных с сималенда
                 [org.apache.httpcomponents/httpclient "4.5"]

                 [com.cemerick/friend "0.2.1"]
                 [clj-time "0.9.0"]
                 [com.draines/postal "1.11.3"]
                 [cider/cider-nrepl "0.9.1"]

                 ;; CLOJURESCRIPT LIBRARIES
                 [org.clojure/clojurescript "0.0-3308"]
                 ]


  :jvm-opts ["-Xmx500m" "-server"]
  ;;:jvm-opts ^:replace ["-Xmx700m" "-server"]


  :plugins [[lein-ring "0.9.4"]
            [lein-ancient "0.6.7"] ;; Для обновления бибилиотек
            [lein-kibit "0.1.2"]   ;; Для рефакторинга
            ]

                                        ; Enable the lein hooks for: clean, compile, test, and jar.
  ;;;:hooks [leiningen.cljsbuild]

  ;; все компилируем
  ;; :aot :all
  ;; отключаем исходники
  ;; :omit-source true

  :ring {:handler newvoyage.handler/app
         :init newvoyage.handler/init
         :auto-reload? true
         :auto-refresh? false
         :nrepl {:start? false}
         }

  :clean-targets ^{:protect false} ["resources/public/js/c" "target"]

  :cljsbuild {
              :builds [{:id "rb-dev"
                        :source-paths ["src-cljs" "src-cljs-lib-ixinfestor"]
                        :compiler {:output-to  "resources/public/js/c/ix/main.js" ; default: target/cljsbuild-main.js
                                   :output-dir "resources/public/js/c/ix"
                                   :asset-path "/js/c/ix"
                                   :main ixinfestor.page_main
                                   :optimizations :none
                                   :source-map true}}

                       {:id "rb-prod"
                        :source-paths ["src-cljs"]
                        :compiler {:output-to  "resources/public/js/c/ix/main.js"
                                   :asset-path "/js/c/ix"
                                   :main ixinfestor.page_main
                                   :pretty-print false
                                   :optimizations :advanced ;; мощная оптимизация, но начинаются проблемы с внешними либами
                                   :externs ["externs/jquery-1.9.js" "externs/tarsonis.js"]
                                   }}
                       ]}

  :profiles {:dev
             {:dependencies [[refactor-nrepl "1.1.0"]
                             [org.clojure/tools.nrepl "0.2.10"]
                             [cider/cider-nrepl "0.9.1"]
                             ]

              :plugins [[lein-cljsbuild "1.0.6"]
                        [com.cemerick/clojurescript.test "0.3.3"]]}


             ;; activated automatically during uberjar
             ;; :uberwar {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             ;; :war     {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             ;; :uberjar {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             ;; :jar     {:aot :all
             ;;           ;;:aot [domdom]
             ;;           :omit-source true
             ;;           }
             }


  )
